function dial_mixeur_closed() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();


	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();	
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_Gloup;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_FR_1);
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_KR_1);
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_EN_1);
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_SP_1);
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_AR_1);
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_RU_1);
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_CN_1);
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_PT_1);
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_IT_1);
				}	
				
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_DE_1);
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_FA_1);
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_TR_1);
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_JP_1);
				}		
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_mixeur_closed_VN_1);
				}					
			}
		}
	
	global.old_target_id = id;


}
