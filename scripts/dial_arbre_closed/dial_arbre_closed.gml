function dial_arbre_closed() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();


	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();	
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_Hiboux;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_arbre_closed_FR_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_FR_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_FR_3);
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_KR_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_KR_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_KR_3);
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_EN_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_EN_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_EN_3);
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_SP_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_SP_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_SP_3);
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait,global.dial_arbre_closed_AR_1 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_AR_2 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_AR_3 );
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait,global.dial_arbre_closed_RU_1 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_RU_2 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_RU_3 );
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait,global.dial_arbre_closed_CN_1 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_CN_2 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_CN_3 );
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait,global.dial_arbre_closed_PT_1 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_PT_2 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_PT_3 );
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait,global.dial_arbre_closed_IT_1 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_IT_2 );
				scr_AddDialogLine(portrait,global.dial_arbre_closed_IT_3 );
				}
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_DE_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_DE_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_DE_3);
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_FA_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_FA_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_FA_3);
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_TR_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_TR_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_TR_3);
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_JP_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_JP_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_JP_3);
				}
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_arbre_closed_VN_1);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_VN_2);
				scr_AddDialogLine(portrait, global.dial_arbre_closed_VN_3);
				}			
			}
		}
	
	global.old_target_id = id;


}
