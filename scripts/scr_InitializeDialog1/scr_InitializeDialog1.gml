function scr_InitializeDialog1() {

	padding = (room_width / 15); // Padding on x Axis between box and window

	// Outer Box Properties (white)
	//width  = window_get_width() - (padding * 2);

	width  = room_width - (padding * 2);
	height = (room_height * 0.3); //200;
	xOrigin = padding;
	yOrigin = room_height / 108;

	// Inner Box Properties (Black)
	borderSize = room_height / 54 ;
	innerBoxWidth  = width - borderSize;
	innerBoxHeight = height - borderSize;
	innerBox_xOrigin = xOrigin + (borderSize / 2);
	innerBox_yOrigin = yOrigin + (borderSize / 2);

	// Dialog Display Properties
	avatarScale = (room_height/720)* 2;
	avatar_xOrigin = innerBox_xOrigin + (room_height / 48);
	avatar_yOrigin = innerBox_yOrigin + (room_height / 36);

	text_xOrigin = avatar_xOrigin   + (room_height / 4.8);
	text_yOrigin = innerBox_yOrigin + (room_height / 50); //28.8
	
	// largeur des lignes en pxl avant retour à la ligne depuis le prochain espace vide (ne fonctionne pas pour les traductions sans espace).
	text_x_width = 1350;

	scr_Initialize_Dialog2();




}
