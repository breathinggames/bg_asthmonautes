function scr_DrawDialog() {
	if (active)
		{
		scr_Switch_Font();
		draw_set_alpha(0.5);
		draw_set_halign(fa_left);
	
		// calcul de la hauteur de la boite de dialogue.
		var hauteur_boite = string_height_ext(stringToDisplay, 90, text_x_width) +  (room_height / 50) * 2;
	
		// cadre blanc
		draw_set_color(c_white);
		if (hauteur_boite < height)
			{
			draw_rectangle(xOrigin, yOrigin, xOrigin + width, yOrigin + height, false);
			}
		else
			{
			draw_rectangle(xOrigin, yOrigin, xOrigin + width, yOrigin + hauteur_boite +  (room_height / 50), false);
			}

		// cadre gris
		draw_set_alpha(0.75);
		draw_set_color(c_black);
		if (hauteur_boite < height)
			{	
			draw_rectangle(innerBox_xOrigin, innerBox_yOrigin, innerBox_xOrigin + innerBoxWidth, innerBox_yOrigin + innerBoxHeight, false);
			}
		else
			{
			draw_rectangle(innerBox_xOrigin, innerBox_yOrigin, innerBox_xOrigin + innerBoxWidth, innerBox_yOrigin + hauteur_boite + (room_height / 50) - borderSize, false);
			}
		
		// texte
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_set_valign(fa_top);
		
		// pour régler le sens de lecture.
		if (global.langage == "arabe") or (global.langage == "farsi")
			{
			draw_set_halign(fa_right);
			marge_alignement = text_x_width;
			}
		else
			{
			draw_set_halign(fa_left);
			marge_alignement = 0
			}
		
		// pour le retour à la ligne
		if (global.langage == "japonais") or (global.langage == "chinois")
			{		
			scr_Retour_a_la_ligne();
			}
			
			
		draw_text_ext(text_xOrigin + marge_alignement, text_yOrigin, stringToDisplay, 90, text_x_width);

		try
			 {
			draw_sprite_ext(spriteToDisplay, -1, avatar_xOrigin, avatar_yOrigin, avatarScale, avatarScale, 0, c_white, 1);
			 }
		catch( _exception)
			{
			show_debug_message(_exception.message);
			show_debug_message(_exception.longMessage);
			show_debug_message(_exception.script);
			show_debug_message(_exception.stacktrace);
			}
		}

}
