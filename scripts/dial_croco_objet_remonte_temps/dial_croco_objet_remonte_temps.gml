function dial_croco_objet_remonte_temps() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_Croco;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FR_1);		
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FR_2);				
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FR_3);						
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FR_4);								
				scr_AddDialogLine(portrait, "...");			
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_KR_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_KR_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_KR_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_KR_4);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_EN_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_EN_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_EN_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_EN_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_SP_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_SP_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_SP_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_SP_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_AR_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_AR_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_AR_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_AR_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_RU_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_RU_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_RU_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_RU_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_CN_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_CN_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_CN_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_CN_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_PT_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_PT_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_PT_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_PT_4);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_IT_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_IT_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_IT_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_IT_4);
				scr_AddDialogLine(portrait, "...");	
				}			
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_DE_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_DE_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_DE_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_DE_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FA_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FA_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FA_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_FA_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_TR_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_TR_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_TR_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_TR_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_JP_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_JP_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_JP_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_JP_4);
				scr_AddDialogLine(portrait, "...");	
				}			
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_VN_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_VN_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_VN_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_remonte_temps_VN_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		}
	
	global.old_target_id = id;


}
