function dial_croco_clic_direct_05() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_Croco;	
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_croco_05_FR_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_FR_2);		
				scr_AddDialogLine(portrait, global.dial_croco_05_FR_3);				
				scr_AddDialogLine(portrait, global.dial_croco_05_FR_4);						
				scr_AddDialogLine(portrait, "...");			
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_KR_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_KR_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_KR_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_KR_4);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_EN_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_EN_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_EN_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_EN_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_SP_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_SP_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_SP_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_SP_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_AR_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_AR_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_AR_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_AR_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_RU_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_RU_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_RU_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_RU_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_CN_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_CN_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_CN_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_CN_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_PT_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_PT_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_PT_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_PT_4);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_IT_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_IT_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_IT_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_IT_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_DE_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_DE_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_DE_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_DE_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_FA_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_FA_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_FA_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_FA_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_TR_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_TR_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_TR_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_TR_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_JP_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_JP_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_JP_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_JP_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_croco_05_VN_1);
				scr_AddDialogLine(portrait, global.dial_croco_05_VN_2);
				scr_AddDialogLine(portrait, global.dial_croco_05_VN_3);
				scr_AddDialogLine(portrait, global.dial_croco_05_VN_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		}
	
	global.old_target_id = id;


}
