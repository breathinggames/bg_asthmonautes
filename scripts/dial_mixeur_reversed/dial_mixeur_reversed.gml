function dial_mixeurersed() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();


	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();	
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_narrateur;
		
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_mixeurersed_FR_1);
				scr_AddDialogLine(portrait, global.dial_mixeurersed_FR_2);		
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_KR_1)
				scr_AddDialogLine(portrait, global.dial_mixeurersed_KR_2)
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_EN_1)
				scr_AddDialogLine(portrait, global.dial_mixeurersed_EN_2)
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_SP_1)
				scr_AddDialogLine(portrait, global.dial_mixeurersed_SP_2)
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait,global.dial_mixeurersed_AR_1  );
				scr_AddDialogLine(portrait,global.dial_mixeurersed_AR_2  );
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_RU_1 );
				scr_AddDialogLine(portrait, global.dial_mixeurersed_RU_2);
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait,global.dial_mixeurersed_CN_1  );
				scr_AddDialogLine(portrait,global.dial_mixeurersed_CN_2  );
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait,global.dial_mixeurersed_PT_1  );
				scr_AddDialogLine(portrait,global.dial_mixeurersed_PT_2  );
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait,global.dial_mixeurersed_IT_1 ); 
				scr_AddDialogLine(portrait,global.dial_mixeurersed_IT_2 );
				}	
				
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_DE_1);
				scr_AddDialogLine(portrait, global.dial_mixeurersed_DE_2);
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_FA_1);
				scr_AddDialogLine(portrait, global.dial_mixeurersed_FA_2);
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_TR_1);
				scr_AddDialogLine(portrait, global.dial_mixeurersed_TR_2);
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_JP_1);
				scr_AddDialogLine(portrait, global.dial_mixeurersed_JP_2);
				}	
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_mixeurersed_VN_1);
				scr_AddDialogLine(portrait, global.dial_mixeurersed_VN_2);
				}					
			}
		}
	
	global.old_target_id = id;


}
