function sc_room_check_destroy() {
	global.particule_destroy_objets = 0;

	sc_destroy_mayo();
	sc_destroy_de_12();
	sc_destroy_certificat();
	sc_destroy_parchemin();
	sc_destroy_trophee();
	sc_destroy_feu();
	sc_destroy_usine();
	sc_destroy_flovent_1();
	sc_destroy_flovent_2();
	sc_destroy_antibiotique();
	sc_destroy_soupe();
	sc_destroy_chambre_inhalation_1();
	sc_destroy_chambre_inhalation_2();
	sc_destroy_tylenol();
	sc_destroy_ventoline_1();
	sc_destroy_ventoline_2();
	sc_destroy_ventoline_3();
	sc_destroy_masque();
	sc_destroy_pollen();
	sc_destroy_anti_allergique();
	sc_destroy_eau();
	sc_destroy_boite_01();
	sc_destroy_boite_02();
	sc_destroy_remonte_temps();
	sc_destroy_sport_metal();
	sc_destroy_sport_chaussure();


}
