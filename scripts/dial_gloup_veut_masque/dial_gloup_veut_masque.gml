function dial_gloup_veut_masque() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();
		
	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = global.portrait_gloup;
		
			if (global.langage == "français")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FR_1);				
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FR_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FR_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FR_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FR_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FR_6);		
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_KR_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_KR_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_KR_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_KR_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_KR_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_KR_6);		
				}	
			
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_EN_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_EN_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_EN_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_EN_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_EN_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_EN_6);		
				}	
			
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_SP_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_SP_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_SP_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_SP_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_SP_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_SP_6);		
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_AR_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_AR_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_AR_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_AR_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_AR_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_AR_6);		
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_RU_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_RU_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_RU_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_RU_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_RU_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_RU_6);		
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_CN_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_CN_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_CN_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_CN_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_CN_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_CN_6);		
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_PT_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_PT_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_PT_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_PT_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_PT_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_PT_6);		
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_IT_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_IT_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_IT_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_IT_4);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_IT_5);		
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_IT_6);		
				}			
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_DE_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_DE_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_DE_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_DE_4);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_DE_5);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_DE_6);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FA_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FA_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FA_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FA_4);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FA_5);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_FA_6);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_TR_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_TR_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_TR_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_TR_4);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_TR_5);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_TR_6);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_JP_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_JP_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_JP_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_JP_4);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_JP_5);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_JP_6);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_VN_1);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_VN_2);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_VN_3);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_VN_4);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_VN_5);
				scr_AddDialogLine(portrait, global.dial_gloup_veut_masque_VN_6);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		} 
	
	global.old_target_id = id;


}
