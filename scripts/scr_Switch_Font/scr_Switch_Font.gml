// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_Switch_Font(){
	if (global.langage == "français")
		{		
		draw_set_font(fnt_Dialog);
		}
	else if (global.langage == "coréen")
		{
		draw_set_font(fnt_coreen_Dialog);	
		}				
	else if (global.langage == "anglais")
		{
		draw_set_font(fnt_Dialog);
		}		
	else if (global.langage == "espagnol")
		{
		draw_set_font(fnt_Dialog);
		}		
	else if (global.langage == "arabe")
		{
		draw_set_font(fnt_arab);
		}						
	else if (global.langage == "russe")
		{
		draw_set_font(fnt_russe_Dialog);
		}	
	else if (global.langage == "chinois")
		{
		draw_set_font(fnt_chinois);
		}	
	else if (global.langage == "portugais")
		{
		draw_set_font(fnt_Dialog);
		}					
	else if (global.langage == "italien")
		{
		draw_set_font(fnt_Dialog);
		}
// autres langues
	else if (global.langage == "allemand")
		{
		draw_set_font(fnt_Dialog);
		}
	else if (global.langage == "farsi")
		{
		draw_set_font(fnt_farsi);
		}
	else if (global.langage == "turc")
		{
		draw_set_font(fnt_turc);
		}
	else if (global.langage == "japonais")
		{
		draw_set_font(fnt_japonais_dialogue);
		}
	else if (global.langage == "vietnamien")
		{
		draw_set_font(fnt_vietnamien);
		}			
}