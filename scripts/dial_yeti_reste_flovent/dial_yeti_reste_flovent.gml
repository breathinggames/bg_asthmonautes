function dial_yeti_reste_flovent() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = Yeti.portrait;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_FR_1);		
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_KR_1);	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_EN_1);
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_SP_1);
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_AR_1);
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_RU_1);
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_CN_1);
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_PT_1);
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_IT_1);
				}		
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_DE_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_FA_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_TR_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_JP_1);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_reste_flovent_VN_1);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		}
	
	global.old_target_id = id;


}
