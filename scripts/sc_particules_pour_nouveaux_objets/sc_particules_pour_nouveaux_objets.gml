function sc_particules_pour_nouveaux_objets() {
	repeat(50)
		{
		my_particule = instance_create_depth(x, y , -500, Particule);
		my_particule.image_blend = make_color_hsv(irandom(255), 255, 255);;
		my_particule.image_xscale = 1.5;
		my_particule.image_yscale = 1.5;
		with my_particule
			{
			motion_add(random(360),random_range(5,8))
			}
		}


}
