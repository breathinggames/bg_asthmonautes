function dial_hiboux_explique_inventaire() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_Hiboux;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_FR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_FR_2);		
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_FR_3);				
				scr_AddDialogLine(portrait, "...");				
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_KR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_KR_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_KR_3);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_EN_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_EN_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_EN_3);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_SP_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_SP_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_SP_3);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_AR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_AR_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_AR_3);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_RU_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_RU_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_RU_3);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_CN_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_CN_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_CN_3);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_PT_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_PT_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_PT_3);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_IT_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_IT_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_IT_3);
				scr_AddDialogLine(portrait, "...");	
				}			
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_DE_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_DE_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_DE_3);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_FA_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_FA_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_FA_3);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_TR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_TR_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_TR_3);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_JP_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_JP_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_JP_3);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_VN_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_VN_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_inventaire_VN_3);
				scr_AddDialogLine(portrait, "...");	
				}							
			}
		}
	
	global.old_target_id = id;


}
