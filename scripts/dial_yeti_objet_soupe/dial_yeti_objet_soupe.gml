function dial_yeti_objet_soupe() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{	
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = Yeti.portrait;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_FR_1);	
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_FR_2);
				scr_AddDialogLine(portrait, "...");			
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_KR_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_KR_2);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_EN_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_EN_2);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_SP_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_SP_2);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_AR_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_AR_2);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_RU_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_RU_2);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_CN_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_CN_2);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_PT_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_PT_2);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_IT_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_IT_2);
				scr_AddDialogLine(portrait, "...");	
				}		
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_DE_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_DE_2);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_FA_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_FA_2);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_TR_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_TR_2);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_JP_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_JP_2);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_VN_1);
				scr_AddDialogLine(portrait, global.dial_yeti_objet_soupe_VN_2);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		}
	
	global.old_target_id = id;


}
