function dial_distributeur_description() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();	
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_distributeur;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_distributeur_description_FR_1);		
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_KR_1);	
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_EN_1);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_SP_1);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_AR_1);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_RU_1);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_CN_1);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_PT_1);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_IT_1);
				scr_AddDialogLine(portrait, "...");	
				}									
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_DE_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_FA_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_TR_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_JP_1);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_distributeur_description_VN_1);
				scr_AddDialogLine(portrait, "...");	
				}					
			}
		}
	
	global.old_target_id = id;


}
