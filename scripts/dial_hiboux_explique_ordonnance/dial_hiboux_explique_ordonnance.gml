function dial_hiboux_explique_ordonnance() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;	
			portrait = sp_portrait_Hiboux;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FR_2);		
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FR_3);				
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FR_4);						
				scr_AddDialogLine(portrait, "...");			
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_KR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_KR_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_KR_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_KR_4);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_EN_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_EN_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_EN_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_EN_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_SP_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_SP_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_SP_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_SP_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_AR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_AR_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_AR_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_AR_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_RU_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_RU_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_RU_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_RU_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_CN_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_CN_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_CN_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_CN_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_PT_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_PT_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_PT_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_PT_4);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_IT_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_IT_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_IT_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_IT_4);
				scr_AddDialogLine(portrait, "...");	
				}			
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_DE_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_DE_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_DE_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_DE_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FA_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FA_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FA_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_FA_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_TR_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_TR_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_TR_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_TR_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_JP_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_JP_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_JP_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_JP_4);
				scr_AddDialogLine(portrait, "...");	
				}			
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_VN_1);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_VN_2);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_VN_3);
				scr_AddDialogLine(portrait, global.dial_hiboux_explique_ordonnance_VN_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		}
	
	global.old_target_id = id;


}
