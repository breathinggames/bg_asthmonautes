function dial_croco_objet_anti_allergique() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;
			portrait = sp_portrait_Croco;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FR_1);		
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FR_2);				
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FR_3);						
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FR_4);								
				scr_AddDialogLine(portrait, "...");			
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_KR_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_KR_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_KR_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_KR_4);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_EN_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_EN_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_EN_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_EN_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_SP_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_SP_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_SP_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_SP_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_AR_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_AR_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_AR_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_AR_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_RU_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_RU_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_RU_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_RU_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_CN_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_CN_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_CN_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_CN_4);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_PT_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_PT_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_PT_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_PT_4);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_IT_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_IT_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_IT_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_IT_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_DE_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_DE_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_DE_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_DE_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FA_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FA_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FA_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_FA_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_TR_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_TR_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_TR_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_TR_4);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_JP_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_JP_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_JP_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_JP_4);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "vietnamiens")
				{
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_VN_1);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_VN_2);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_VN_3);
				scr_AddDialogLine(portrait, global.dial_croco_objet_anti_allergique_VN_4);
				scr_AddDialogLine(portrait, "...");	
				}						
			}
		}
	
	global.old_target_id = id;


}
