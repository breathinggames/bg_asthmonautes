function dial_peluche_still_need_vento() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;	
			portrait = Peluche.portrait;
		
			if (global.langage == "français")
				{		
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_FR_1);
				scr_AddDialogLine(portrait, "...");			
				}
			else if (global.langage == "coréen")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_KR_1);
				scr_AddDialogLine(portrait, "...");	
				}				
			else if (global.langage == "anglais")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_EN_1);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "espagnol")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_SP_1);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "arabe")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_AR_1);
				scr_AddDialogLine(portrait, "...");	
				}						
			else if (global.langage == "russe")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_RU_1);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "chinois")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_CN_1);
				scr_AddDialogLine(portrait, "...");	
				}	
			else if (global.langage == "portugais")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_PT_1);
				scr_AddDialogLine(portrait, "...");	
				}					
			else if (global.langage == "italien")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_IT_1);
				scr_AddDialogLine(portrait, "...");	
				}				
			// Ajout des nouvelles langues
			else if (global.langage == "allemand")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_DE_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "farsi")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_FA_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "turc")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_TR_1);
				scr_AddDialogLine(portrait, "...");	
				}
			else if (global.langage == "japonais")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_JP_1);
				scr_AddDialogLine(portrait, "...");	
				}		
			else if (global.langage == "vietnamien")
				{
				scr_AddDialogLine(portrait, global.dial_peluche_still_need_vento_VN_1);
				scr_AddDialogLine(portrait, "...");	
				}					
			}
		}
	
	global.old_target_id = id;


}
