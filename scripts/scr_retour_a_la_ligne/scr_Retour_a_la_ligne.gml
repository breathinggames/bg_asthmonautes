// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_Retour_a_la_ligne()
	{
	// quelques essais ici pour jouer sur les retours à la ligne
	// j'arrive à saisir la longueur de ma chaine de caractère.
	if (global.cut_stringToDisplay == "todo") // and (global.longueur > 0)
		{
		global.longueur = string_length(stringToDisplay);

		if (global.longueur > global.position_caractere_pour_retour_a_la_ligne)
			{
			stringToDisplay = string_insert("\n", stringToDisplay, global.position_caractere_pour_retour_a_la_ligne);		
			}
		if (global.longueur > global.position_caractere_pour_retour_a_la_ligne*2)
			{
			stringToDisplay = string_insert("\n", stringToDisplay, global.position_caractere_pour_retour_a_la_ligne*2);		
			}
		if (global.longueur > global.position_caractere_pour_retour_a_la_ligne*3)
			{
			stringToDisplay = string_insert("\n", stringToDisplay, global.position_caractere_pour_retour_a_la_ligne*3);		
			}
		if (global.longueur > global.position_caractere_pour_retour_a_la_ligne*4)
			{
			stringToDisplay = string_insert("\n", stringToDisplay, global.position_caractere_pour_retour_a_la_ligne*4);		
			}
		if (global.longueur > global.position_caractere_pour_retour_a_la_ligne*5)
			{
			stringToDisplay = string_insert("\n", stringToDisplay, global.position_caractere_pour_retour_a_la_ligne*5);		
			}
		global.cut_stringToDisplay = "done";
		}
	}