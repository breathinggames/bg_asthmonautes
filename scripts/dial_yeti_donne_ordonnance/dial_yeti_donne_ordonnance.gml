function dial_yeti_donne_ordonnance() {
	// est ce que je coupe le dialogue en cours pour donner la priorité à un nouveau personnage sur lequel je clic ?
	scr_cut_dialogue();

	with (oDialog)
		{
		if (!active)
			{
			scr_InitializeDialog1();			
			active = true;
			dialogNum  = 0; // This needs to be reset at beginning of each conversation
			convoIndex = 0;		
			portrait = Yeti.portrait;
		
			scr_AddDialogLine(portrait, global.dial_yeti_donne_ordonnance_UNIVERSAL_1);
			scr_AddDialogLine(portrait, "...");			
	
			}
		}
	
	global.old_target_id = id;


}
