function sc_init_global_variables_pnj() {
	// variable pour les objets
	sc_reinit_all_objects("keep");


	// pour identifier les cibles des clics, et savoir quand réunitialiser les boites de dialogue.
	global.current_target_id = 0;
	global.old_target_id = 0;

	// win game ?
	global.ok_to_go_to_ending = 0;

	// pour les clic directs sur les personnages :
	global.bambou_presentation = 0;
	global.bambou_texte_clic_index = 1;

	global.gloup_presentation = 0;
	global.gloup_texte_clic_index = 1;

	global.distributeur_presentation = 0;
	global.distributeur_texte_clic_index = 1;

	global.croco_presentation = 0;
	global.croco_texte_clic_index = 1;

	global.porcinet_presentation = 0;
	global.porcinet_texte_clic_index = 1;

	global.pantin_presentation = 0;
	global.pantin_texte_clic_index = 1;
	
	global.cut_stringToDisplay = "todo";


	// initialisation variable de Champi :
	global.champi_ventoline_taken = 0; // passe à 1 si ventoline.
	global.champi_chambre_inhalation_taken = 0; 
	global.champi_effort_already_done = 0; 
	global.champi_connaissance_du_probleme = 0;
	global.champi_traitement_trop_tard = 0;
	global.champi_ventoline_et_chambre_inhalation_taken = 0; 
	global.champi_sport_metal_taken = 0;
	global.champi_sport_chaussure_taken = 0;
	global.champi_remonte_temps_taken = 0;
	global.champi_success = 0;
	// activation des besoins pour les quêtes des objets sportifs.
	global.champi_need_eau = 0;
	global.champi_need_chaussures = 0;
	global.champi_need_barre = 0;
	global.champi_need_pouloeuf = 0;
	global.champi_equipement_sportif = 0;

	global.champi_portrait = sp_portrait_Champi;

	// initialisation variables Peluche :
	global.peluche_ventoline_taken = 0; // passe à 1 si ventoline.
	global.peluche_Chambre_inhalation_1_taken = 0; 
	global.peluche_ventoline_et_Chambre_inhalation_1_taken = 0; 
	global.peluche_anti_allergique_taken = 0;
	global.peluche_success = 0;
	global.peluche_give_parchemin = 0;

	// initialisation variables Yéti
	global.yeti_rhume = 1; // passe à 0 si ventoline + traitement quotidien.
	global.yeti_ventoline_taken = 0; // passe à 1 si ventoline.
	global.yeti_flovent_taken = 0; // passe à 1 si traitement.
	global.yeti_success = 0;
	global.yeti_possede_certificat = 1;

	// initialisation des variable de Gloup :
	global.gloup_masque_taken = 0;
	global.portrait_gloup = sp_portrait_Gloup;
	global.sprite_index_gloup = sp_Gloup;

	// initialisation des variables de Bambou
	global.bambou_mise_en_garde_urne = 0;// vide pour le moment.

	// initialisation des variables de Croco.
	global.croco_need_soupe = 0;// vide pour le moment.

	// initialisation des variables de Hiboux
	global.hibouxerse_mixer = 0;
	global.hiboux_clic_deverouillage = 0;

	// variables de pantin
	global.pantin_propose_mayo = 0;

	// à propos des portes


	// terminal de distribution des médicaments;
	global.ordonnance_absorbee = 0;
	global.parchemin_absorbee = 0;
	
	// pour les langages sans coupure, il est nécessaire de gérer les retours à la lignes par le code.
	global.position_caractere_pour_retour_a_la_ligne = 32;
	
}
