///sc_particule_destroy_objet();
function sc_particule_destroy_objet() {

	// particules sur la porte

	if (global.particule_destroy_objets == 1)
		{
		var nombre_de_particules;
		var my_image_blend;
		var my_scale;
		var go_particule = "beaucoup";


		// valeur de clic par défaut, mais ça peut changer plus bas.
		nombre_de_particules = 100;
		my_scale = 3;	
		my_image_blend = make_color_hsv(irandom(255), 255, 255);

		repeat(nombre_de_particules)
			{
			my_particule = instance_create_depth(mouse_x, mouse_y, Cache_ouverture_porte.depth-1, Particule);
			my_particule.image_blend = my_image_blend;
			my_particule.image_xscale = my_scale;
			my_particule.image_yscale = my_scale;
			with my_particule
				{
				motion_add(random(360),random_range(5,8))
				}
			}
		}


}
