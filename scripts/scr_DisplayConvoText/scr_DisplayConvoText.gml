function scr_DisplayConvoText() 
	{
	// le code joue cette fonction en continu, de la première étape à la dernière

	// ici, le code ne lance cette étape qu'une seule fois, au charement de chaque nouvelle étape de dialogue.
	if (currCharIndex < string_length(dialog[convoIndex, 1]) + 1)
		{
		global.cut_stringToDisplay = "todo";
		spriteToDisplay = dialog[convoIndex, 0];
		stringToDisplay = dialog[convoIndex, 1];
		currCharIndex = string_length(dialog[convoIndex, 1]) + 1;
		}
		
	// ici, le code n'y va que lorsque la première étape d'un nouveau dialogue est chargé
	else
		{
		if (keyboard_check_pressed(vk_space)) or mouse_check_button_pressed(mb_left)
			{
			convoIndex++;
			stringToDisplay = "";
			currCharIndex   = 1;
		
			if (convoIndex == convoDialogCount)
				{
				active = false;
			
				// reset de l'array2D contenant les dialogues.
				scr_Initialize_Dialog2();			
				}
			}	
		}
	}
