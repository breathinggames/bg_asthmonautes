function sc_destroy_flovent_1() {

	if (global.destroy_flovent_1 == 1)
		{
		sc_particule_destroy_objet();		
		Cache_ouverture_porte.state = "flash";	
		if (global.particule_destroy_objets == 1)
			{
			audio_play_sound(sd_object_disapearance,10,0);
			}
		with Flovent_1
			{
			instance_destroy();	
			}
	
		// petit ajout... tout mon système de suppression d'objet est devenu obsolette du fait que les objets ne sont plus présenté au sol...
		global.destroy_flovent_1 = 0;
		}


}
