function dial_clic_direct(argument0) {

	// argument0 = global.bambou_texte_clic_index;


	if (oDialog.active == true)
		{
		if (oDialog.convoIndex == oDialog.convoDialogCount -1)
			{
			oDialog.active = false;
			// comment savoir qu'une boite de dialogue en est à son dernier encart ?
			}
		}				

	if (oDialog.active == false)
		{	
		var my_id = object_get_name(object_index);
		switch my_id
			{
			case "Bambou": 	
				switch (argument0)
					{
					case 1:	dial_bambou_clic_direct_01(); break;
					case 2:	dial_bambou_clic_direct_02(); break;
					case 3:	dial_bambou_clic_direct_03(); break;
					case 4:	dial_bambou_clic_direct_04(); break;
					case 5:	dial_bambou_clic_direct_05(); break;
					case 6:	dial_bambou_clic_direct_06(); break;
					}
				break;

			case "Terminal": 	
				switch (argument0)
					{
					case 1:	dial_distributeur_clic_direct_01(); break;
					case 2:	dial_distributeur_clic_direct_02(); break;
					case 3:	dial_distributeur_clic_direct_03(); break;
					case 4:	dial_distributeur_clic_direct_04(); break;
					case 5:	dial_distributeur_clic_direct_05(); break;
					case 6:	dial_distributeur_clic_direct_06(); break;
					}
				break;
			
			case "Gloup": 	
				switch (argument0)
					{
					case 1:	dial_gloup_clic_direct_01(); break;
					case 2:	dial_gloup_clic_direct_02(); break;
					case 3:	dial_gloup_clic_direct_03(); break;
					case 4:	dial_gloup_clic_direct_04(); break;
					case 5:	dial_gloup_clic_direct_05(); break;
					case 6:	dial_gloup_clic_direct_06(); break;
					}
				break;
			
			case "Croco": 	
				switch (argument0)
					{
					case 1:	dial_croco_clic_direct_01(); break;
					case 2:	dial_croco_clic_direct_02(); break;
					case 3:	dial_croco_clic_direct_03(); break;
					case 4:	dial_croco_clic_direct_04(); break;
					case 5:	dial_croco_clic_direct_05(); break;
					case 6:	dial_croco_clic_direct_06(); break;
					}
				break;
			
			case "Porcinet": 	
				switch (argument0)
					{
					case 1:	dial_porcinet_clic_direct_01(); break;
					case 2:	dial_porcinet_clic_direct_02(); break;
					case 3:	dial_porcinet_clic_direct_03(); break;
					case 4:	dial_porcinet_clic_direct_04(); break;
					case 5:	dial_porcinet_clic_direct_05(); break;
					case 6:	dial_porcinet_clic_direct_06(); break;
					}
				break;
			
			case "Pantin": 	
				switch (argument0)
					{
					case 1:	dial_pantin_clic_direct_01(); break;
					case 2:	dial_pantin_clic_direct_02(); break;
					case 3:	dial_pantin_clic_direct_03(); break;
					case 4:	dial_pantin_clic_direct_04(); break;
					case 5:	dial_pantin_clic_direct_05(); break;
					case 6:	dial_pantin_clic_direct_06(); break;
					}
				break;	
				
			case "Peluche": 	
				switch (argument0)
					{
					case 1:	dial_peluche_clic_direct_01(); break;
					case 2:	dial_peluche_clic_direct_02(); break;
					case 3:	dial_peluche_clic_direct_03(); break;
					case 4:	dial_peluche_clic_direct_04(); break;
					case 5:	dial_peluche_clic_direct_05(); break;
					case 6:	dial_peluche_clic_direct_06(); break;
					}
				break;	
			default: break;
			}

		argument0 ++;	
		if (argument0 > 6)
			{
			argument0 = 1;	
			}					
		}				
	return(argument0);


}
