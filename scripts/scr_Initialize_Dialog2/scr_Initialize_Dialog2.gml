function scr_Initialize_Dialog2() {
	for (i = 0; i < 100; i++)
	{
		dialog[i, 0] = -1; // Sprite Index
		dialog[i, 1] = ""; // Convo Dialog
	}

	active = false;
	convoDialogCount = 0;  // Numer of Lines in a specific conversation
	convoIndex       = 0;  // Current index towards our convoDialogCount
	spriteToDisplay  = -1; // Avatar To Display
	stringToDisplay  = ""; // Conversation line to display
	currCharIndex    = 1;  // Current character index of string to apply to stringToDisplay
}
