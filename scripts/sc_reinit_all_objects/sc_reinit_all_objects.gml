function sc_reinit_all_objects(argument0) {
	// sc_reinit_all_objects(état de l'objet)
	//argument 0 = état de l'objet (0 : on ne détruit pas  /  1 : on détruit)
	if (argument0 == "keep") var state = 0;
	if (argument0 == "kill") var state = 1;

	global.combo_vento_cdi_1 = 0;
	global.combo_vento_cdi_2 = 0;

	global.destroy_mayo = state;
	global.destroy_de_12 = state;
	global.destroy_certificat = state;
	global.destroy_parchemin = state;
	global.destroy_trophee = state;
	global.destroy_feu = state;
	global.destroy_usine = state;
	global.destroy_flovent_1 = state;
	global.destroy_flovent_2 = state;
	global.destroy_antibiotique = state;
	global.destroy_soupe = state;
	global.destroy_chambre_inhalation_1 = state;
	global.destroy_chambre_inhalation_2 = state;
	global.destroy_tylenol = state;
	global.destroy_ventoline_1 = state;
	global.destroy_ventoline_2 = state;
	global.destroy_ventoline_3 = state;
	global.destroy_masque = state;
	global.destroy_pollen = state;
	global.destroy_anti_allergique = state;
	global.destroy_eau = state; // room 1;
	global.destroy_boite_01 = state;
	global.destroy_boite_02 = state;
	global.destroy_remonte_temps = state;
	global.destroy_sport_metal = state;
	global.destroy_sport_chaussure = state;
	global.destroy_vento_et_chambre_inhalation = state;


}
