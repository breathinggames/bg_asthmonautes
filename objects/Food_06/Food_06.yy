{
  "spriteId": {
    "name": "sp_food_06",
    "path": "sprites/sp_food_06/sp_food_06.yy",
  },
  "solid": false,
  "visible": true,
  "managed": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "Element_decor_parent",
    "path": "objects/Element_decor_parent/Element_decor_parent.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Éléments de décoration",
    "path": "folders/Objects/Éléments de décoration.yy",
  },
  "resourceVersion": "1.0",
  "name": "Food_06",
  "tags": [],
  "resourceType": "GMObject",
}