/// @description Insert description here
// You can write your code in this editor


if (state == "come on")
	{
	// si cache ouverture porte pas en activité
	if (Cache_ouverture_porte.state == "off")
		{
		if (image_alpha == 0)
			{
			// petite rustine si le temps laissé au cache ouverture porte n'est pas suffisant pour ramener le dpeth porte à la bonne valeur. Utile si le joueur est très rapide pour éviter un effet d'affichage malheureux.
			if instance_exists(Porte_parent)
				{
				with Porte_parent
					{
					depth = global.depth_porte
					}
				}
		
			my_colour = make_color_hsv(irandom(255), 255, 255);
			audio_play_sound(sd_teleport,10,0);
			audio_sound_pitch(sd_teleport,random_range(0.8,1.2));
			repeat(100) //100
				{
			
				var bandeau_transition = instance_create_depth(random(room_width), random(room_height), depth-1, Transition_room);
				bandeau_transition.en_bordure = 0;
				}
			
			with oDialog
				{
				scr_InitializeDialog1();
				}			
			
			sc_schuffle_list_music();
			if (audio_is_playing(global.global_bk_music_01))
				{
				audio_sound_gain(global.global_bk_music_01, 0, 1000);	
				}
			if (audio_is_playing(global.global_bk_music_02))
				{
				audio_sound_gain(global.global_bk_music_02, 0, 1000);	
				}
			}
		if (image_alpha < 1)
			{
			image_alpha += facteur_fade;
			}
		else 
			{
			if (room == room_title) // pour couper spécifiquement les musiques qui se lancent depuis le logo.
				{
				if (audio_is_playing(global.music_logo_01))
					{
					audio_stop_sound(global.music_logo_01);
					}
				if (audio_is_playing(global.music_logo_02))
					{
					audio_stop_sound(global.music_logo_02);	
					}					
				}
			
			if (audio_is_playing(global.global_bk_music_01))
				{
				audio_stop_sound(global.global_bk_music_01);
				}
			if (audio_is_playing(global.global_bk_music_02))
				{
				audio_stop_sound(global.global_bk_music_02);	
				}
			if (audio_is_playing(sd_asthma_attack))
				{
				audio_stop_sound(sd_asthma_attack);	
				}

			state = "back off";			
			room_goto(target);
			}
		}
	}

else if (state == "back off")
	{
	if (image_alpha > 0)
		{
		image_alpha -= facteur_fade;	
		}
	else
		{		
		if (facteur_fade != facteur_fade_init)
			{
			facteur_fade = facteur_fade_init;	
			}
		state = "off";	
		}
	}