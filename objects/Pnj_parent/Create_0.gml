/// @description Insert description here
// You can write your code in this editor
event_inherited();

regulare_size = image_yscale;

balancement = "gauche";
evo_taille = "extension";
evo_taille_facteur = 0.0005;
evo_taille_max = regulare_size * 1.05;
evo_taille_min = regulare_size * 0.95;

limite_angle = 5;

type_survol = "rotation";