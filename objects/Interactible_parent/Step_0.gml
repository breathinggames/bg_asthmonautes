/// @description Insert description here
// You can write your code in this editor

event_inherited();

if (type_survol == "rotation")
	{
	if (survol == 1)
		{
		if (angle_balancement == "gauche")
			{
			if (image_angle > angle_min)
				{
				image_angle -= angle_facteur	
				}
			else
				{
				angle_balancement = "droite";
				}
			}
		else if (angle_balancement == "droite")
			{
			if (image_angle < angle_max)
				{
				image_angle += angle_facteur;	
				}
			else
				{
				angle_balancement = "gauche";
				}
			}
		}
	
	if (survol == 0)
		{
		image_angle = 0;
		}
	}
	
else if (type_survol == "zoom")
	{
	if (survol == 1)
		{
		if (type_zoom == "out")
			{
			if (image_xscale > image_scale_min)
				{
				image_xscale -= image_scale_facteur;
				image_yscale -= image_scale_facteur;
				}
			else
				{
				type_zoom = "in";
				}
			}
		else if (type_zoom == "in")
			{
			if (image_xscale < image_scale_max)
				{
				image_xscale += image_scale_facteur;
				image_yscale += image_scale_facteur;
				}
			else
				{
				type_zoom = "out";
				}
			}
		}
	
	if (survol == 0)
		{
		image_xscale = image_xscale_reference;
		image_yscale = image_yscale_reference;
		}		
	}
	
