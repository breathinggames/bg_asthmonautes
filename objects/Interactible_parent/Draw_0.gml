/// @description Insert description here
// You can write your code in this editor

draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
//draw_set_font(fnt_description_objet);
scr_Switch_Font_description();
draw_set_halign(fa_center);

var my_x = room_width/2;
var my_y = room_height/10 *7;


draw_set_color(c_black);

if (global.langage == "farsi") or (global.langage == "japonais")  or (global.langage == "turc")
	{
	draw_text(my_x+1, my_y+1, texte_informatif);
	draw_text(my_x+1, my_y, texte_informatif);
	draw_text(my_x+1, my_y-1, texte_informatif);
	draw_text(my_x, my_y-1, texte_informatif);
	draw_text(my_x-1, my_y-1, texte_informatif);
	draw_text(my_x-1, my_y, texte_informatif);
	draw_text(my_x-1, my_y+1, texte_informatif);
	draw_text(my_x, my_y+1, texte_informatif);
	}

else
	{

	draw_text(my_x+2, my_y+2, texte_informatif);
	draw_text(my_x+2, my_y, texte_informatif);
	draw_text(my_x+2, my_y-2, texte_informatif);
	draw_text(my_x, my_y-1, texte_informatif);
	draw_text(my_x-2, my_y-2, texte_informatif);
	draw_text(my_x-2, my_y, texte_informatif);
	draw_text(my_x-2, my_y+2, texte_informatif);
	draw_text(my_x, my_y+2, texte_informatif);
	}

draw_set_color(c_white);
draw_text(my_x, my_y, texte_informatif);