/// @description Insert description here
// You can write your code in this editor

event_inherited();

survol = 0;

//type_survol = "rotation";
angle_balancement = "gauche";
angle_facteur = 1.5;
angle_max = 3.5;
angle_min = -3.5;


//type_survol = "zoom";
type_zoom = "in";
image_xscale_reference = image_xscale;
image_yscale_reference = image_yscale;
image_scale_min = image_xscale_reference * 0.98;
image_scale_max = image_xscale_reference * 1.02;
image_scale_facteur = 0.0025;

texte_informatif = "";