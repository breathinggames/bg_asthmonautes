/// @description Insert description here
// You can write your code in this editor

global.cloud_limit = 4;

event_inherited();

// flottement horizontal
global.x_reference = x;
global.x_reference_depart = global.x_reference;

global.x_amplitude_seuil_haut = global.x_reference_depart - 75;
global.x_amplitude_seuil_bas = global.x_reference_depart + 75;
global.facteur_hspeed = 0 * randomise_h_flottement;;

global.hspeed_max = 0;//0.15;
global.hspeed_min = 0;//-0.15;


// flottement vertical
global.y_reference = y;
global.y_reference_depart = global.y_reference;

global.y_amplitude_seuil_haut = global.y_reference_depart - 40;
global.y_amplitude_seuil_bas =global.y_reference_depart + 40;
global.facteur_vspeed = 0 * randomise_v_flottement;;

global.vspeed_max = 0;// 0.15;
global.vspeed_min = 0;// -0.15;

global.global_bk_music_01 = audio_play_sound(ds_list_find_value(global.list_music_ile_02,0), 10, 1);
global.global_bk_music_02 = audio_play_sound(ds_list_find_value(global.list_music_ile_02,1), 10, 1);
