/// @description Insert description here
// You can write your code in this editor

if (global.langage == "français")
	{		
	texte = global.texte_bouton_credit_FR;
	}
else if (global.langage == "coréen")
	{
	texte = global.texte_bouton_credit_KR;
	}				
else if (global.langage == "anglais")
	{
	texte = global.texte_bouton_credit_EN;
	}		
else if (global.langage == "espagnol")
	{
	texte = global.texte_bouton_credit_SP;
	}		
else if (global.langage == "arabe")
	{
	texte = global.texte_bouton_credit_AR;
	}						
else if (global.langage == "russe")
	{
	texte = global.texte_bouton_credit_RU;
	}	
else if (global.langage == "chinois")
	{
	texte = global.texte_bouton_credit_CN;
	}	
else if (global.langage == "portugais")
	{
	texte = global.texte_bouton_credit_PT;
	}					
else if (global.langage == "italien")
	{
	texte = global.texte_bouton_credit_IT;
	}	
// autres langues
else if (global.langage == "allemand")
	{
	texte = global.texte_bouton_credit_DE;
	}	
else if (global.langage == "farsi")
	{
	texte = global.texte_bouton_credit_FA;
	}	
else if (global.langage == "turc")
	{
	texte = global.texte_bouton_credit_TR;
	}	
else if (global.langage == "japonais")
	{
	texte = global.texte_bouton_credit_JP;
	}	
else if (global.langage == "vietnamien")
	{
	texte = global.texte_bouton_credit_VN;
	}	