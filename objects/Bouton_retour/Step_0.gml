if (global.langage == "français")
	{		
	texte = global.texte_bouton_retour_FR;
	}
else if (global.langage == "coréen")
	{
	texte = global.texte_bouton_retour_KR;
	}				
else if (global.langage == "anglais")
	{
	texte = global.texte_bouton_retour_EN;
	}		
else if (global.langage == "espagnol")
	{
	texte = global.texte_bouton_retour_SP;
	}		
else if (global.langage == "arabe")
	{
	texte = global.texte_bouton_retour_AR;
	}						
else if (global.langage == "russe")
	{
	texte = global.texte_bouton_retour_RU;
	}	
else if (global.langage == "chinois")
	{
	texte = global.texte_bouton_retour_CN;
	}	
else if (global.langage == "portugais")
	{
	texte = global.texte_bouton_retour_PT;
	}					
else if (global.langage == "italien")
	{
	texte = global.texte_bouton_retour_IT;
	}	
	
// autre langages
else if (global.langage == "allemand")
	{
	texte = global.texte_bouton_retour_DE;
	}						
else if (global.langage == "farsi")
	{
	texte = global.texte_bouton_retour_FA;
	}	
else if (global.langage == "turc")
	{
	texte = global.texte_bouton_retour_TR;
	}	
else if (global.langage == "japonais")
	{
	texte = global.texte_bouton_retour_JP;
	}					
else if (global.langage == "vietnamien")
	{
	texte = global.texte_bouton_retour_VN;
	}	
