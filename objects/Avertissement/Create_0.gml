/// @description Insert description here
// You can write your code in this editor

x = room_width / 2;
y = room_height / 2;

depth = global.depth_credits;

if (global.langage == "français")
	{		
	text = global.mise_en_garde_FR;
	}
else if (global.langage == "coréen")
	{
	text = global.mise_en_garde_KR;
	}				
else if (global.langage == "anglais")
	{
	text = global.mise_en_garde_EN;
	}		
else if (global.langage == "espagnol")
	{
	text = global.mise_en_garde_SP;
	}		
else if (global.langage == "arabe")
	{
	text = global.mise_en_garde_AR;
	}						
else if (global.langage == "russe")
	{
	text = global.mise_en_garde_RU;
	}	
else if (global.langage == "chinois")
	{
	text = global.mise_en_garde_CN;
	}	
else if (global.langage == "portugais")
	{
	text = global.mise_en_garde_PT;
	}					
else if (global.langage == "italien")
	{
	text = global.mise_en_garde_IT;
	}
//
// autre langages
else if (global.langage == "allemand")
	{
	text = global.mise_en_garde_DE;
	}						
else if (global.langage == "farsi")
	{
	text = global.mise_en_garde_FA;
	}	
else if (global.langage == "turc")
	{
	text = global.mise_en_garde_TR;
	}	
else if (global.langage == "japonais")
	{
	text = global.mise_en_garde_JP;
	}					
else if (global.langage == "vietnamien")
	{
	text = global.mise_en_garde_VN;
	}	