
var connaissance = 0; 

// dialogue de base
// présentation des objets
if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_porcinet_objet_mayo();
		connaissance ++;
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_porcinet_objet_de_12();
		connaissance ++;
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_porcinet_objet_parchemin();
		connaissance ++;
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_porcinet_objet_trophee();
		connaissance ++;
		}
	}
	
if (instance_exists(Flovent))
	{
	if (Flovent.taken == 2)
		{
		dial_porcinet_objet_flovent();
		connaissance ++;
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_porcinet_objet_antibiotique();
		connaissance ++;
		}
	}
	
if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		dial_porcinet_objet_soupe();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_1))
	{
	if (Chambre_inhalation_1.taken == 2)
		{
		dial_porcinet_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_2))
	{
	if (Chambre_inhalation_2.taken == 2)
		{
		dial_porcinet_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_porcinet_objet_tylenol();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_1))
	{
	if (Ventoline_1.taken == 2)
		{
		dial_porcinet_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_2))
	{
	if (Ventoline_2.taken == 2)
		{
		dial_porcinet_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_3))
	{
	if (Ventoline_3.taken == 2)
		{
		dial_porcinet_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Vento_et_chambre_inhalation))
	{
	if (Vento_et_chambre_inhalation.taken == 2)
		{
		dial_porcinet_objet_vento_et_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Masque))
	{
	if (Masque.taken == 2)
		{
		dial_porcinet_objet_masque();
		connaissance ++;
		}
	}
	
if (instance_exists(Anti_allergique))
	{
	if (Anti_allergique.taken == 2)
		{
		dial_porcinet_objet_anti_allergique();
		connaissance ++;
		}
	}
	
if (instance_exists(Eau))
	{
	if (Eau.taken == 2)
		{
		dial_porcinet_objet_eau();
		connaissance ++;
		}
	}

if (instance_exists(Remonte_temps))
	{
	if (Remonte_temps.taken == 2)
		{
		dial_porcinet_objet_remonte_temps();
		connaissance ++;
		}
	}

if (instance_exists(Sport_metal))
	{
	if (Sport_metal.taken == 2)
		{
		dial_porcinet_objet_sport_metal();
		connaissance ++;
		}
	}

if (instance_exists(Sport_chaussure))
	{
	if (Sport_chaussure.taken == 2)
		{
		dial_porcinet_objet_sport_chaussure();
		connaissance ++;
		}
	}
// fin dialogue de base

// fin présentation des objets
if (connaissance == 0)
	{
	if (global.porcinet_presentation == 0)
		{
		dial_porcinet_01();
		global.porcinet_presentation = 1;
		}
	else
		{
		global.porcinet_texte_clic_index = dial_clic_direct(global.porcinet_texte_clic_index);
		}
	}