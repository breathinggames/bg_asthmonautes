if (image_index == 0)
	{
	image_xscale = 1.5;
	image_yscale = 1.5;
	draw_set_color(c_black);	
	}
else
	{
	draw_set_color(c_white);		
	if (image_index == 1)
		{
		image_xscale = 1.6;
		image_yscale = 1.6;
		}
	if (image_index == 2)
		{
		image_xscale = 1.45;
		image_yscale = 1.45;
		}
	}

draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,c_white,1);
draw_set_valign(fa_middle);
draw_text(x,y-sprite_height/2, texte);