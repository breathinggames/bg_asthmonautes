/// @description Insert description here
// You can write your code in this editor

event_inherited();

sc_particules_pour_nouveaux_objets();

takeable = 1;

x_inventory = 0;

type_survol = "rotation";

aura_launched = 0;

my_image_xscale = 1;
my_image_yscale = 1;

go_alpha = 0;

room_spawn = room;
x_spawn = x;
y_spawn = y;
persistent = 1;
image_alpha = 0;

alarm[0] = 1;