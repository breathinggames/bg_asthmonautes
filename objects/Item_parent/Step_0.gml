/// @description Insert description here
// You can write your code in this editor


// taken 4 = en mouvement. 
// Taken 3 = en utilisation. 
// Taken 2 = en utilisation après relâchement du premier clic. 
// Taken 1 = dans l'inventaire. Taken 0 = sur la scène.

event_inherited();

if (taken == 0)
	{
	if (room_spawn == room) and (go_alpha == 1)
		{
		if (image_alpha == 0)
			{
			image_alpha = 1;
			x = x_spawn;
			y = y_spawn;
			}
		}
	else
		{
		image_alpha = 0;
		}
	}	
	

// sachant que global.y_inventaire change depuis Controller.
if (taken == 1) 
	{
	if (image_alpha == 0)
		{
		image_alpha = 1;
		}
		
	if not (depth == global.depth_objet_taken)
		{
		depth = global.depth_objet_taken;
		}
		
	if not (y == global.y_inventaire)
		{
		if (y < global.y_inventaire - global.vitesse_escamotage_inventaire)
			{
			y += global.vitesse_escamotage_inventaire;	
			}
		else if (y > global.y_inventaire + global.vitesse_escamotage_inventaire)
			{
			y -= global.vitesse_escamotage_inventaire;	
			}
		else
			{
			y = global.y_inventaire;
			}
		}
		
	if not (x == x_inventory)
		{
		if (x < x_inventory - global.vitesse_escamotage_inventaire)
			{
			x += global.vitesse_escamotage_inventaire;	
			}
		else if (x > x_inventory + global.vitesse_escamotage_inventaire)
			{
			x -= global.vitesse_escamotage_inventaire;	
			}
		else
			{
			x = x_inventory;
			}
		}
	}
	
if (taken == 2) or (taken == 3)
	{
	x = mouse_x;
	y = mouse_y;
	texte_informatif = "";	
	}
	
if (taken == 4) and (y > my_item_target.y)
	{	
	taken = 1; // 5?
	speed = 0;
	x = my_item_target.x;
	y = my_item_target.y;
	x_inventory = x;
	
	
	with (my_item_target)
		{
		instance_destroy();
		//y += 5;
		}
	
	}
	
	
// création de l'aura de l'inventaire
if (y > room_height) and (aura_launched == 0) // global.y_inventaire_invisible)
	{
	aura_launched = 1;
	instance_create_depth(x,y,depth, Aura_inventaire);	
	}
	
if (y < room_height) and (aura_launched = 1)
	{
	aura_launched = 0;	
	}
	