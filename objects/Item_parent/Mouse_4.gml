/// @description Insert description here
// You can write your code in this editor


event_inherited();

// taken 4 = en mouvement. 
// Taken 3 = en utilisation. 
// Taken 2 = en utilisation après relâchement du premier clic. 
// Taken 1 = dans l'inventaire. Taken 0 = sur la scène.

if (image_alpha = 1)
	{
	if (taken == 1)
		{
		taken = 3;
		sc_taken_use_on();
		}

	if (taken == 0) and (takeable == 1)
		{
		with mon_ombre
			{
			instance_destroy();	
			}							
			
		taken = 4; 
		hspeed = 0;
		vspeed = 0;
		persistent = true;
		my_item_target = instance_create_depth(x,y,depth, Item_target);
		my_item_target.item = id;
		my_item_target.y = global.y_inventaire_visible;
	
		global.count_inventory = 0;
		with (Item_parent)
			{
			if (taken > 0)
				{
				global.count_inventory ++;	
				}
			}
	
		switch (global.count_inventory)
			{
			case 1: my_item_target.x = global.inventaire_position_1; break;
			case 2: my_item_target.x = global.inventaire_position_2; break;
			case 3: my_item_target.x = global.inventaire_position_3; break;
			case 4: my_item_target.x = global.inventaire_position_4; break;
			case 5: my_item_target.x = global.inventaire_position_5; break;
			case 6: my_item_target.x = global.inventaire_position_6; break;
			case 7: my_item_target.x = global.inventaire_position_7; break;
			case 8: my_item_target.x = global.inventaire_position_8; break;
			case 9: my_item_target.x = global.inventaire_position_9; break;
			case 10: my_item_target.x = global.inventaire_position_10; break;
			case 11: my_item_target.x = global.inventaire_position_11; break;
			case 12: my_item_target.x = global.inventaire_position_12; break;
			}
	
		move_towards_point(my_item_target.x, my_item_target.y, global.vitesse_escamotage_inventaire);
		}
	}	

	
