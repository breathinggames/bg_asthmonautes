if (global.langage == "français")
	{		
	texte = global.texte_bouton_quitter_FR;
	}
else if (global.langage == "coréen")
	{
	texte = global.texte_bouton_quitter_KR;
	}				
else if (global.langage == "anglais")
	{
	texte = global.texte_bouton_quitter_EN;
	}		
else if (global.langage == "espagnol")
	{
	texte = global.texte_bouton_quitter_SP;
	}		
else if (global.langage == "arabe")
	{
	texte = global.texte_bouton_quitter_AR;
	}						
else if (global.langage == "russe")
	{
	texte = global.texte_bouton_quitter_RU;
	}	
else if (global.langage == "chinois")
	{
	texte = global.texte_bouton_quitter_CN;
	}	
else if (global.langage == "portugais")
	{
	texte = global.texte_bouton_quitter_PT;
	}					
else if (global.langage == "italien")
	{
	texte = global.texte_bouton_quitter_IT;
	}	

// autre langages
else if (global.langage == "allemand")
	{
	texte = global.texte_bouton_quitter_DE;
	}						
else if (global.langage == "farsi")
	{
	texte = global.texte_bouton_quitter_FA;
	}	
else if (global.langage == "turc")
	{
	texte = global.texte_bouton_quitter_TR;
	}	
else if (global.langage == "japonais")
	{
	texte = global.texte_bouton_quitter_JP;
	}					
else if (global.langage == "vietnamien")
	{
	texte = global.texte_bouton_quitter_VN;
	}	
