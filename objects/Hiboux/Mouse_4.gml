/// @description Insert description here

var connaissance = 0; 

// dialogue de base
// présentation des objets
if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_hiboux_objet_mayo();
		connaissance ++;
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_hiboux_objet_de_12();
		connaissance ++;
		}
	}
	
if (instance_exists(Certificat))
	{
	if (Certificat.taken == 2)
		{
		dial_hiboux_objet_certificat();
		connaissance ++;
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_hiboux_objet_parchemin();
		connaissance ++;
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_hiboux_objet_trophee();
		connaissance ++;
		}
	}
	
if (instance_exists(Flovent))
	{
	if (Flovent.taken == 2)
		{
		dial_hiboux_objet_flovent();
		connaissance ++;
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_hiboux_objet_antibiotique();
		connaissance ++;
		}
	}
	
if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		dial_hiboux_objet_soupe();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_1))
	{
	if (Chambre_inhalation_1.taken == 2)
		{
		dial_hiboux_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_2))
	{
	if (Chambre_inhalation_2.taken == 2)
		{
		dial_hiboux_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_hiboux_objet_tylenol();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_1))
	{
	if (Ventoline_1.taken == 2)
		{
		dial_hiboux_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_2))
	{
	if (Ventoline_2.taken == 2)
		{
		dial_hiboux_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_3))
	{
	if (Ventoline_3.taken == 2)
		{
		dial_hiboux_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Vento_et_chambre_inhalation))
	{
	if (Vento_et_chambre_inhalation.taken == 2)
		{
		dial_hiboux_objet_vento_et_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Masque))
	{
	if (Masque.taken == 2)
		{
		dial_hiboux_objet_masque();
		connaissance ++;
		}
	}
	
if (instance_exists(Anti_allergique))
	{
	if (Anti_allergique.taken == 2)
		{
		dial_hiboux_objet_anti_allergique();
		connaissance ++;
		}
	}
	
if (instance_exists(Eau))
	{
	if (Eau.taken == 2)
		{
		dial_hiboux_objet_eau();
		connaissance ++;
		}
	}

if (instance_exists(Remonte_temps))
	{
	if (Remonte_temps.taken == 2)
		{
		dial_hiboux_objet_remonte_temps();
		connaissance ++;
		}
	}

if (instance_exists(Sport_metal))
	{
	if (Sport_metal.taken == 2)
		{
		dial_hiboux_objet_sport_metal();
		connaissance ++;
		}
	}

if (instance_exists(Sport_chaussure))
	{
	if (Sport_chaussure.taken == 2)
		{
		dial_hiboux_objet_sport_chaussure();
		connaissance ++;
		}
	}
// fin dialogue de base

if (global.champi_need_pouloeuf == 1)
	{
	dial_hiboux_mixeur_pour_mayo();
	connaissance ++;
	global.hibouxerse_mixer = 1;
	}


if (instance_exists(Masque))
	{
	dial_hiboux_explique_inventaire();
	connaissance ++;
	}
	
if (instance_exists(Certificat))
	{
	dial_hiboux_explique_ordonnance();
	connaissance ++;
	}	
	
// fin présentation des objets
if (connaissance == 0)
	{
	if (global.yeti_success == 0)
	and (global.peluche_success == 0)
	and (global.champi_success == 0)
		{
		dial_hiboux_explication_mission();
		if not (global.mixeur_statut == "open")
			{
			if (global.hiboux_clic_deverouillage == 0)
				{
				global.hiboux_clic_deverouillage = 1; // ouverture de la porte, ça se passe en clic gauche controler pour la suite.
				//global.mixeur_statut = "opening";
				}
			}
		}
	
	else if (global.yeti_success == 1)
	and (global.peluche_success == 0)
	and (global.champi_success == 0)
		{
		dial_hiboux_reste_peluche_et_champi();
		}
	
	else if (global.yeti_success == 1)
	and (global.peluche_success == 1)
	and (global.champi_success == 0)
		{
		dial_hiboux_reste_champi();
		}
	
	else if (global.yeti_success == 1)
	and (global.peluche_success == 1)
	and (global.champi_success == 1)
		{
		global.ok_to_go_to_ending = 1;
		dial_hiboux_win_game();
		}
	}
	
