/// @description Insert description here
// You can write your code in this editor

event_inherited();

// je fais la transition en deux étapes. Il faut d'abord que la boite de dialogue soit fermée.
if (global.ok_to_go_to_ending == 1)
	{
	if not (oDialog.active == true)
		{
		global.ok_to_go_to_ending = 2;
		Cache_transition_room.target = room_ending;		
		Cache_transition_room.state = "come on";
		}
	}