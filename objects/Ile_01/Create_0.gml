/// @description Insert description here
// You can write your code in this editor


global.cloud_limit = 5;

event_inherited();

// flottement horizontal
global.x_reference = x;
global.x_reference_depart = global.x_reference;

global.x_amplitude_seuil_haut = global.x_reference_depart - 50;
global.x_amplitude_seuil_bas = global.x_reference_depart + 50;
global.facteur_hspeed = 0.05 * randomise_h_flottement;

global.hspeed_max = 0.3;
global.hspeed_min = -0.3;


// flottement vertical
global.y_reference = y;
global.y_reference_depart = global.y_reference;

global.y_amplitude_seuil_haut = global.y_reference_depart - 35;
global.y_amplitude_seuil_bas =global.y_reference_depart + 35;
global.facteur_vspeed = 0.05 * randomise_v_flottement;

global.vspeed_max = 0.2;
global.vspeed_min = -0.2;

