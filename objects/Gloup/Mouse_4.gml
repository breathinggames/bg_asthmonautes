

var connaissance = 0; 

if (global.champi_need_eau == 1)
	{
	if not (instance_exists(Eau))
		{
		dial_gloup_donne_eau();
		instance_create_depth(x,y+75, depth, Eau);	
		global.champi_need_eau = 2;
		connaissance ++;
		}
	}

// pnj obtient le masque
if (global.gloup_masque_taken == 0)
	{
	if (instance_exists(Masque))
		{
		if (Masque.taken == 2)
			{
			global.destroy_masque = 1;
			sc_destroy_masque();
			sc_rearrange_objects();
			global.gloup_masque_taken = 1;
			global.sprite_index_gloup = sp_Gloup_done;
			sprite_index = global.sprite_index_gloup;
			global.portrait_gloup = sp_portrait_Gloup_done;
			connaissance ++;
			}
		}
	}
	
if (global.gloup_masque_taken  == 1)
	{
	if not (global.arbre_statut == "open")
		{
		dial_gloup_content();
		connaissance ++;
		global.arbre_statut = "opening";
		}	
	}
	
	
// présentation des objets

if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_gloup_objet_mayo();
		connaissance ++;
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_gloup_objet_de_12();
		connaissance ++;
		}
	}
	
if (instance_exists(Certificat))
	{
	if (Certificat.taken == 2)
		{
		dial_gloup_objet_certificat();
		connaissance ++;
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_gloup_objet_parchemin();
		connaissance ++;
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_gloup_objet_trophee();
		connaissance ++;
		}
	}
	
if (instance_exists(Flovent))
	{
	if (Flovent.taken == 2)
		{
		dial_gloup_objet_flovent();
		connaissance ++;
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_gloup_objet_antibiotique();
		connaissance ++;
		}
	}
	
if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		dial_gloup_objet_soupe();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_1))
	{
	if (Chambre_inhalation_1.taken == 2)
		{
		dial_gloup_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_2))
	{
	if (Chambre_inhalation_2.taken == 2)
		{
		dial_gloup_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_gloup_objet_tylenol();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_1))
	{
	if (Ventoline_1.taken == 2)
		{
		dial_gloup_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_2))
	{
	if (Ventoline_2.taken == 2)
		{
		dial_gloup_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_3))
	{
	if (Ventoline_3.taken == 2)
		{
		dial_gloup_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Vento_et_chambre_inhalation))
	{
	if (Vento_et_chambre_inhalation.taken == 2)
		{
		dial_gloup_objet_vento_et_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Masque))
	{
	if (Masque.taken == 2)
		{
		dial_gloup_objet_masque();
		connaissance ++;
		}
	}
	
if (instance_exists(Anti_allergique))
	{
	if (Anti_allergique.taken == 2)
		{
		dial_gloup_objet_anti_allergique();
		connaissance ++;
		}
	}
	
if (instance_exists(Eau))
	{
	if (Eau.taken == 2)
		{
		dial_gloup_objet_eau();
		connaissance ++;
		}
	}

if (instance_exists(Remonte_temps))
	{
	if (Remonte_temps.taken == 2)
		{
		dial_gloup_objet_remonte_temps();
		connaissance ++;
		}
	}

if (instance_exists(Sport_metal))
	{
	if (Sport_metal.taken == 2)
		{
		dial_gloup_objet_sport_metal();
		connaissance ++;
		}
	}

if (instance_exists(Sport_chaussure))
	{
	if (Sport_chaussure.taken == 2)
		{
		dial_gloup_objet_sport_chaussure();
		connaissance ++;
		}
	}
	
// quête
if (global.arbre_statut == "closed")
	{
	dial_gloup_veut_masque();
	}
	

// clics directs
else
	{
	if (connaissance == 0)
		{
		global.gloup_texte_clic_index = dial_clic_direct(global.gloup_texte_clic_index);
		}
	}
	
