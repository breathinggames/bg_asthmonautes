/// @description Insert description here
// You can write your code in this editor

if (state == "flash")
	{
	if (image_alpha == 0)
		{
		image_alpha = 1;
		state = "flash off";
		}
	}
	
else if (state == "flash off")
	{
	if (image_alpha > 0)	
		{
		image_alpha -= facteur_fade*2;
		}
	else
		{
		state = "off";	
		}
	}

if (state == "come on")
	{
	if (image_alpha == 0)
		{
		ile_parent_memory_vspeed = Ile_parent.vspeed;
		ile_parent_memory_hspeed = Ile_parent.hspeed;
		Ile_parent.vspeed = 0;
		Ile_parent.hspeed = 0;
		
		audio_play_sound(sd_door_opening,10,0);
		audio_sound_gain(sd_door_opening, 1, 0);
		
		
		with Flottants
			{
			vspeed = 0;
			hspeed = 0;				
			}
			
		my_colour = make_color_hsv(irandom(255), 255, 255);
		//audio_play_sound(sd_teleport,10,0);
		//audio_sound_pitch(sd_teleport,random_range(0.8,1.2));
		repeat(100) //100
			{
			var bandeau_transition = instance_create_depth(random(room_width), random(room_height), depth-1, Ouverture_porte);
			bandeau_transition.en_bordure = 0;
			}
		}
	if (image_alpha < 1.75)
		{
		image_alpha += facteur_fade;
		}
	else
		{			
		state = "back off";			
		}
	}

else if (state == "back off")
	{
	if (image_alpha > 0)
		{
		image_alpha -= facteur_fade;	
		
		switch global.door_to_unlock
			{
			case "Mixeur_b": 
				var x_particules = Mixeur_b.x;
				var y_particules = Mixeur_b.y -100;
				break;
			case "Arbre_a":
				var x_particules = Arbre_a.x;
				var y_particules = Arbre_a.y -100;
				break;	
			case "Oeuf_a":
				var x_particules = Oeuf_a.x;
				var y_particules = Oeuf_a.y -100;
				break;			
			case "Trou_a":
				var x_particules = Trou_a.x;
				var y_particules = Trou_a.y -100;				
				break;
			}			
		
		// particules sur la porte
		var nombre_de_particules;
		var my_image_blend;
		var my_scale;
		var go_particule = "beaucoup";


		// valeur de clic par défaut, mais ça peut changer plus bas.
		nombre_de_particules = 5;
		my_scale = 2;	
		my_image_blend = make_color_hsv(irandom(255), 255, 255);

		repeat(nombre_de_particules)
			{
			my_particule = instance_create_depth(x_particules, y_particules, Cache_ouverture_porte.depth-1, Particule);
			my_particule.image_blend = my_image_blend;
			my_particule.image_xscale = my_scale;
			my_particule.image_yscale = my_scale;
			with my_particule
				{
				motion_add(random(360),random_range(5,8))
				}
			}			
		
		}
	else
		{		
		if (facteur_fade != facteur_fade_init)
			{
			facteur_fade = facteur_fade_init;	
			}
		state = "off";	
		
		audio_sound_gain(sd_door_opening, 0, 2000);
		
		Ile_parent.vspeed = ile_parent_memory_vspeed;
		Ile_parent.hspeed = ile_parent_memory_hspeed;
		
		alarm[0] = room_speed*6;
		switch global.door_to_unlock
			{
			case "Mixeur_b": 
				global.mixeur_statut = "open";
				Mixeur_b.image_blend = c_white;
				break;
			case "Arbre_a":
				global.arbre_statut = "open";
				Arbre_a.image_blend = c_white;
				break;
			case "Oeuf_a":
				global.oeuf_statut = "open";
				Oeuf_a.image_blend = c_white;
				break;
			case "Trou_a":
				global.urne_bois_statut = "open";	
				Trou_a.image_blend = c_white;
				break;
			}		
		}
	}