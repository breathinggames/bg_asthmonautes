
var connaissance = 0; 

// dialogue de base
// présentation des objets
if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_croco_objet_mayo();
		connaissance ++;
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_croco_objet_de_12();
		connaissance ++;
		}
	}
	
if (instance_exists(Certificat))
	{
	if (Certificat.taken == 2)
		{
		dial_croco_objet_certificat();
		connaissance ++;
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_croco_objet_parchemin();
		connaissance ++;
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_croco_objet_trophee();
		connaissance ++;
		}
	}
	
if (instance_exists(Flovent))
	{
	if (Flovent.taken == 2)
		{
		dial_croco_objet_flovent();
		connaissance ++;
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_croco_objet_antibiotique();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_1))
	{
	if (Chambre_inhalation_1.taken == 2)
		{
		dial_croco_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Chambre_inhalation_2))
	{
	if (Chambre_inhalation_2.taken == 2)
		{
		dial_croco_objet_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_croco_objet_tylenol();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_1))
	{
	if (Ventoline_1.taken == 2)
		{
		dial_croco_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_2))
	{
	if (Ventoline_2.taken == 2)
		{
		dial_croco_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Ventoline_3))
	{
	if (Ventoline_3.taken == 2)
		{
		dial_croco_objet_ventoline();
		connaissance ++;
		}
	}
	
if (instance_exists(Vento_et_chambre_inhalation))
	{
	if (Vento_et_chambre_inhalation.taken == 2)
		{
		dial_croco_objet_vento_et_chambre_inhalation();
		connaissance ++;
		}
	}
	
if (instance_exists(Anti_allergique))
	{
	if (Anti_allergique.taken == 2)
		{
		dial_croco_objet_anti_allergique();
		connaissance ++;
		}
	}
	
if (instance_exists(Eau))
	{
	if (Eau.taken == 2)
		{
		dial_croco_objet_eau();
		connaissance ++;
		}
	}

if (instance_exists(Remonte_temps))
	{
	if (Remonte_temps.taken == 2)
		{
		dial_croco_objet_remonte_temps();
		connaissance ++;
		}
	}

if (instance_exists(Sport_metal))
	{
	if (Sport_metal.taken == 2)
		{
		dial_croco_objet_sport_metal();
		connaissance ++;
		}
	}

if (instance_exists(Sport_chaussure))
	{
	if (Sport_chaussure.taken == 2)
		{
		dial_croco_objet_sport_chaussure();
		connaissance ++;
		}
	}
// fin dialogue de base

if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		global.destroy_soupe = 1;
		sc_destroy_soupe();
		sc_rearrange_objects();			
		dial_croco_recoit_soupe();
		connaissance ++;
		instance_create_depth(x,y+50, depth, Sport_metal);
		global.champi_need_barre = 2;
		}
	}

if (global.champi_need_barre == 1)
	{
	dial_croco_demande_soupe();
	connaissance ++;
	if (global.croco_need_soupe == 0)
		{
		global.croco_need_soupe = 1;
		}
	}
	
// fin présentation des objets
if (connaissance == 0)
	{
	if (global.croco_presentation == 0)
		{
		dial_croco_01(); // présentation générale du rôle de Croco.	
		global.croco_presentation = 1;
		}
	else
		{
		global.croco_texte_clic_index = dial_clic_direct(global.croco_texte_clic_index);
		}
	}