/// @description Insert description here
// You can write your code in this editor

global.cloud_limit = 1;

event_inherited();

// flottement horizontal
global.x_reference = x;
global.x_reference_depart = global.x_reference;

global.x_amplitude_seuil_haut = global.x_reference_depart - 100;
global.x_amplitude_seuil_bas = global.x_reference_depart + 100;
global.facteur_hspeed = 0.02 * randomise_h_flottement;;

global.hspeed_max = 0.15;
global.hspeed_min = -0.15;


// flottement vertical
global.y_reference = y;
global.y_reference_depart = global.y_reference;

global.y_amplitude_seuil_haut = global.y_reference_depart - 20;
global.y_amplitude_seuil_bas =global.y_reference_depart + 20;
global.facteur_vspeed = 0.01 * randomise_v_flottement;;

global.vspeed_max = 0.1;
global.vspeed_min = -0.1;

global.global_bk_music_01 = audio_play_sound(ds_list_find_value(global.list_music_ile_03,0), 10, 1);
global.global_bk_music_02 = audio_play_sound(ds_list_find_value(global.list_music_ile_03,1), 10, 1);

