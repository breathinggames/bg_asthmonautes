/// @description Insert description here
// You can write your code in this editor

//draw_set_font(fnt_Dialog);
scr_Switch_Font();

draw_set_halign(fa_center);
draw_set_valign(fa_top);

if (global.langage == "français")
	{		
	text = global.credits_FR;
	}
else if (global.langage == "coréen")
	{
	text = global.credits_KR;
	}				
else if (global.langage == "anglais")
	{
	text = global.credits_EN;
	}		
else if (global.langage == "espagnol")
	{
	text = global.credits_SP;
	}		
else if (global.langage == "arabe")
	{
	text = global.credits_AR;
	}						
else if (global.langage == "russe")
	{
	text = global.credits_RU;
	}	
else if (global.langage == "chinois")
	{
	text = global.credits_CN;
	}	
else if (global.langage == "portugais")
	{
	text = global.credits_PT;
	}					
else if (global.langage == "italien")
	{
	text = global.credits_IT;
	}	
// autres langues
else if (global.langage == "allemand")
	{
	text = global.credits_DE;
	}
else if (global.langage == "farsi")
	{
	text = global.credits_FA;
	}
else if (global.langage == "turc")
	{
	text = global.credits_TR;
	}
else if (global.langage == "japonais")
	{
	text = global.credits_JP;
	}
else if (global.langage == "vietnamien")
	{
	text = global.credits_VN;
	}		

draw_text_ext_color(x,y,text,75, (room_width -room_width/10), c_black, c_black, c_black, c_black, 1);