/// @description Insert description here
// You can write your code in this editor

event_inherited();

//texte_informatif = "Mme Yeti";

if (global.langage == "français")
	{		
	texte_informatif = global.desc_Yeti_FR;
	}
else if (global.langage == "coréen")
	{
	texte_informatif = global.desc_Yeti_KR;
	}				
else if (global.langage == "anglais")
	{
	texte_informatif = global.desc_Yeti_EN;
	}		
else if (global.langage == "espagnol")
	{
	texte_informatif = global.desc_Yeti_SP;
	}		
else if (global.langage == "arabe")
	{
	texte_informatif = global.desc_Yeti_AR;
	}						
else if (global.langage == "russe")
	{
	texte_informatif = global.desc_Yeti_RU;
	}	
else if (global.langage == "chinois")
	{
	texte_informatif = global.desc_Yeti_CN;
	}	
else if (global.langage == "portugais")
	{
	texte_informatif = global.desc_Yeti_PT;
	}					
else if (global.langage == "italien")
	{
	texte_informatif = global.desc_Yeti_IT;
	}	
// autre langages
else if (global.langage == "allemand")
	{
	texte_informatif = global.desc_Yeti_DE;
	}						
else if (global.langage == "farsi")
	{
	texte_informatif = global.desc_Yeti_FA;
	}	
else if (global.langage == "turc")
	{
	texte_informatif = global.desc_Yeti_TR;
	}	
else if (global.langage == "japonais")
	{
	texte_informatif = global.desc_Yeti_JP;
	}					
else if (global.langage == "vietnamien")
	{
	texte_informatif = global.desc_Yeti_VN;
	}		