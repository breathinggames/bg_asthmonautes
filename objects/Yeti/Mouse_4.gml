
var connaissance = 0; 

// change state selon objects

// présentation des objets
if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_yeti_objet_mayo();
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_yeti_objet_de_12();
		}
	}
	
if (instance_exists(Certificat))
	{
	if (Certificat.taken == 2)
		{
		dial_yeti_objet_certificat();
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_yeti_objet_parchemin();
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_yeti_objet_trophee();
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_yeti_objet_antibiotique();
		}
	}
	
if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		dial_yeti_objet_soupe();
		}
	}
	
if (instance_exists(Chambre_inhalation_1))
	{
	if (Chambre_inhalation_1.taken == 2)
		{
		dial_yeti_objet_chambre_inhalation();
		}
	}
	
if (instance_exists(Chambre_inhalation_2))
	{
	if (Chambre_inhalation_2.taken == 2)
		{
		dial_yeti_objet_chambre_inhalation();
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_yeti_objet_tylenol();
		}
	}
	
if (instance_exists(Vento_et_chambre_inhalation))
	{
	if (Vento_et_chambre_inhalation.taken == 2)
		{
		dial_yeti_objet_vento_et_chambre_inhalation();
		}
	}
	
if (instance_exists(Anti_allergique))
	{
	if (Anti_allergique.taken == 2)
		{
		dial_yeti_objet_anti_allergique();
		}
	}
	
if (instance_exists(Eau))
	{
	if (Eau.taken == 2)
		{
		dial_yeti_objet_eau();
		}
	}

if (instance_exists(Remonte_temps))
	{
	if (Remonte_temps.taken == 2)
		{
		dial_yeti_objet_remonte_temps();
		}
	}

if (instance_exists(Sport_metal))
	{
	if (Sport_metal.taken == 2)
		{
		dial_yeti_objet_sport_metal();
		}
	}

if (instance_exists(Sport_chaussure))
	{
	if (Sport_chaussure.taken == 2)
		{
		dial_yeti_objet_sport_chaussure();
		}
	}
// fin dialogue de base

if (global.yeti_ventoline_taken == 0)
	{
	if (instance_exists(Ventoline_1))
		{
		if (Ventoline_1.taken == 2)
			{
			global.destroy_ventoline_1 = 1;
			sc_destroy_ventoline_1();
			sc_rearrange_objects();
			global.yeti_ventoline_taken = 1;
			}
		}
					
	if (instance_exists(Ventoline_2))
		{
		if (Ventoline_2.taken == 2)
			{
			global.destroy_ventoline_2 = 1;
			sc_destroy_ventoline_2();
			sc_rearrange_objects();
			global.yeti_ventoline_taken = 1;
			}
		}
					
	if (instance_exists(Ventoline_3))
		{
		if (Ventoline_3.taken == 2)
			{
			global.destroy_ventoline_3 = 1;
			sc_destroy_ventoline_3();
			sc_rearrange_objects();
			global.yeti_ventoline_taken = 1;
			}
		}
	}
	
// change state selon objects
if (global.yeti_flovent_taken == 0)
	{
	// ouverture
	if (instance_exists(Flovent_1))
		{	
		if (Flovent_1.taken == 2)
			{
			global.destroy_flovent_1 = 1;
			sc_destroy_flovent_1();
			sc_rearrange_objects();
			global.yeti_flovent_taken = 1;
			}
		}
		
	if (instance_exists(Flovent_2))
		{	
		if (Flovent_2.taken == 2)
			{
			global.destroy_flovent_2 = 1;
			sc_destroy_flovent_2();
			sc_rearrange_objects();
			global.yeti_flovent_taken = 1;
			}
		}		
	}



// trigger dialogue
if (global.yeti_ventoline_taken == 0) and (global.yeti_flovent_taken == 0)
	{
	if not (oDialog.active)
		{
		if (global.yeti_possede_certificat == 1)
			{
			dial_yeti_full_rhume_01();
			instance_create_depth(x,y+100, depth, Certificat); 
			global.yeti_possede_certificat = 0;
			}
		else
			{
			dial_yeti_full_rhume_02();
			}
		}
	}
	
if (global.yeti_ventoline_taken == 1) and (global.yeti_flovent_taken == 0)
	{
	if (audio_is_playing(sd_asthma_attack)) {audio_stop_sound(sd_asthma_attack);}
	dial_yeti_reste_flovent();
	}
	
if (global.yeti_ventoline_taken == 0) and (global.yeti_flovent_taken == 1)
	{
	dial_yeti_reste_vento();
	}
	
if (global.yeti_ventoline_taken == 1) and (global.yeti_flovent_taken == 1)
	{
	global.yeti_success = 1;		
	if (audio_is_playing(sd_asthma_attack)) 
		{
		audio_stop_sound(sd_asthma_attack);
		}
	if (global.oeuf_statut == "closed")
		{
		portrait = sp_portrait_Yeti_content;			
		dial_yeti_cured();
		global.oeuf_statut = "opening";
		}
	else
		{
		dial_yeti_cured_02();
		}
	}
	