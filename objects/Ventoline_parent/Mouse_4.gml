/// @description Insert description here
// You can write your code in this editor

event_inherited();

var combo = 0;
// tentative de code pour combiner les objets :
// chambre d'hinalation
if (instance_exists(Chambre_inhalation_parent))
	{
	if (instance_exists(Chambre_inhalation_1))
		{
		if (Chambre_inhalation_1.taken == 2)
			{
			global.destroy_chambre_inhalation_1 = 1;
			sc_destroy_chambre_inhalation_1();
			combo = 1;
			}
		}
	if (instance_exists(Chambre_inhalation_2))
		{
		if (Chambre_inhalation_2.taken == 2)
			{
			global.destroy_chambre_inhalation_2 = 1;
			sc_destroy_chambre_inhalation_2();
			combo = 1;
			}
		}			
	
	if (combo == 1)
		{		
		sc_rearrange_objects();					

		my_combinaison = instance_create_depth(x, y, global.depth_objet, Vento_et_chambre_inhalation);
	
		if (global.combo_vento_cdi_1 == 0)
			{
			global.combo_vento_cdi_1 = my_combinaison;
			global.combo_vento_cdi_1.taken = 1;
			global.combo_vento_cdi_1.x_inventory = x;
			}
		else
			{
			global.combo_vento_cdi_2 = my_combinaison;
			global.combo_vento_cdi_2.taken = 1;
			global.combo_vento_cdi_2.x_inventory = x;			
			}
		
		if (object_index == Ventoline_1)
			{
			global.destroy_ventoline_1 = 1;
			sc_destroy_ventoline_1();
			}
			
		else if (object_index == Ventoline_2)
			{
			global.destroy_ventoline_2 = 1;
			sc_destroy_ventoline_2();
			}		
			
		else if (object_index == Ventoline_3)
			{
			global.destroy_ventoline_3 = 1;
			sc_destroy_ventoline_3();
			}			
			
		sc_rearrange_objects();		
		}
	}

