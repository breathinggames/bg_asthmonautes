/// @description Insert description here
// You can write your code in this editor

event_inherited();

//texte_informatif = "Boite à médicaments";


if (global.langage == "français")
	{		
	texte_informatif = global.desc_Boite_medicament_FR;
	}
else if (global.langage == "coréen")
	{
	texte_informatif = global.desc_Boite_medicament_KR;
	}				
else if (global.langage == "anglais")
	{
	texte_informatif = global.desc_Boite_medicament_EN;
	}		
else if (global.langage == "espagnol")
	{
	texte_informatif = global.desc_Boite_medicament_SP;
	}		
else if (global.langage == "arabe")
	{
	texte_informatif = global.desc_Boite_medicament_AR;
	}						
else if (global.langage == "russe")
	{
	texte_informatif = global.desc_Boite_medicament_RU;
	}	
else if (global.langage == "chinois")
	{
	texte_informatif = global.desc_Boite_medicament_CN;
	}	
else if (global.langage == "portugais")
	{
	texte_informatif = global.desc_Boite_medicament_PT;
	}					
else if (global.langage == "italien")
	{
	texte_informatif = global.desc_Boite_medicament_IT;
	}	
// autre langages
else if (global.langage == "allemand")
	{
	texte_informatif = global.desc_Boite_medicament_DE;
	}						
else if (global.langage == "farsi")
	{
	texte_informatif = global.desc_Boite_medicament_FA;
	}	
else if (global.langage == "turc")
	{
	texte_informatif = global.desc_Boite_medicament_TR;
	}	
else if (global.langage == "japonais")
	{
	texte_informatif = global.desc_Boite_medicament_JP;
	}					
else if (global.langage == "vietnamien")
	{
	texte_informatif = global.desc_Boite_medicament_VN;
	}	