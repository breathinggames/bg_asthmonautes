/// @description Insert description here
// You can write your code in this editor



event_inherited();

// destroy object
sc_room_check_destroy();

global.particule_destroy_objets = 1;

room_speed = 60;

repeat (irandom_range(1,global.cloud_limit))
	{
	var cloud = instance_create_depth(x,y,150, Cloud);	
	cloud.x = random_range(- Cloud.sprite_width, room_width);
	cloud.y = random_range(0, room_height);
	}
	
switch irandom(1)
	{
	case 0 : randomise_h_flottement = -1; break;
	case 1 : randomise_h_flottement = 1; break;
	}

switch irandom(1)
	{
	case 0 : randomise_v_flottement = -1; break;
	case 1 : randomise_v_flottement = 1; break;
	}
	
depth = global.depth_iles;
