/// @description Insert description here
// You can write your code in this editor

event_inherited();


// création de nuages :
if (instance_number(Cloud) < global.cloud_limit)
	{
	if (alarm[0] == -1)
		{
		alarm[0] = random(room_speed * 6);
		}
	}
	
	
if (Cache_ouverture_porte.state == "off")
	{
	// effets de flottement :
	// flottement horizontal
	global.x_reference += hspeed

	if (global.x_reference > global.x_amplitude_seuil_bas) and (global.facteur_hspeed > 0)
		{
		global.facteur_hspeed = -global.facteur_hspeed;
		}
	
	if (global.x_reference < global.x_amplitude_seuil_haut) and (global.facteur_hspeed < 0)
		{
		global.facteur_hspeed = -global.facteur_hspeed;
		}
	
	// flottement vertical
	global.y_reference += vspeed

	if (global.y_reference > global.y_amplitude_seuil_bas) and (global.facteur_vspeed > 0)
		{
		global.facteur_vspeed = -global.facteur_vspeed;
		}
	
	if (global.y_reference < global.y_amplitude_seuil_haut) and (global.facteur_vspeed < 0)
		{
		global.facteur_vspeed = -global.facteur_vspeed;
		}
	
	

	with(Flottants)
		{	
		if (taken == 0) and (image_alpha = 1)
			{
			if (hspeed > global.hspeed_max)
				{
				hspeed = global.hspeed_max;
				}
			if (hspeed < global.hspeed_min)
				{
				hspeed = global.hspeed_min;
				}
	
			if (vspeed > global.vspeed_max)
				{
				vspeed = global.vspeed_max;
				}
			if (vspeed < global.vspeed_min)
				{
				vspeed = global.vspeed_min;
				}
			vspeed += global.facteur_vspeed;
			hspeed += global.facteur_hspeed;		
			}
		}
	}