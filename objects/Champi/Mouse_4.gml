
var connaissance = 0; 

// dialogue de base
// présentation des objets
if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_champi_objet_mayo();
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_champi_objet_de_12();
		}
	}
	
if (instance_exists(Certificat))
	{
	if (Certificat.taken == 2)
		{
		dial_champi_objet_certificat();
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_champi_objet_parchemin();
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_champi_objet_trophee();
		}
	}
	
if (instance_exists(Flovent))
	{
	if (Flovent.taken == 2)
		{
		dial_champi_objet_flovent();
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_champi_objet_antibiotique();
		}
	}
	
if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		dial_champi_objet_soupe();
		}
	}
	
if (instance_exists(Chambre_inhalation_1))
	{
	if (Chambre_inhalation_1.taken == 2)
		{
		dial_champi_objet_chambre_inhalation();
		}
	}
	
if (instance_exists(Chambre_inhalation_2))
	{
	if (Chambre_inhalation_2.taken == 2)
		{
		dial_champi_objet_chambre_inhalation();
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_champi_objet_tylenol();
		}
	}
	
if (instance_exists(Anti_allergique))
	{
	if (Anti_allergique.taken == 2)
		{
		dial_champi_objet_anti_allergique();
		}
	}

if (instance_exists(Ventoline_1))
	{
	if (Ventoline_1.taken == 2)
		{
		dial_champi_objet_vento();
		}
	}
if (instance_exists(Ventoline_2))
	{
	if (Ventoline_2.taken == 2)
		{
		dial_champi_objet_vento();
		}
	}
if (instance_exists(Ventoline_3))
	{
	if (Ventoline_3.taken == 2)
		{
		dial_champi_objet_vento();
		}
	}
	
	
// fin dialogue de base


// change state selon objects

// premier contact, activation des besoins
if (global.champi_need_eau == 0)
	{
	global.champi_need_eau = 1; // était sur 1
	global.champi_need_chaussures = 1;
	global.champi_need_barre = 1;
	}
	
// si je donne un équipement sportif :
if (global.champi_need_eau == 2) or (global.champi_need_eau == 1) // le 1 est pour faire des tests en plaçant des objets directement dans la scène. Le 2, c'est le cas d'objet reçu de la part de PNJ.
	{
	if (instance_exists(Eau))
		{
		if (Eau.taken == 2)
			{
			global.destroy_eau = 1;
			sc_destroy_eau();
			sc_rearrange_objects();
			dial_champi_prend_eau();
			global.champi_need_eau = 3;
			}
		}
	}

if (global.champi_need_barre == 2) or (global.champi_need_barre == 1)
	{
	if (instance_exists(Sport_metal))
		{
		if (Sport_metal.taken == 2)
			{
			global.destroy_sport_metal = 1;
			sc_destroy_sport_metal();
			sc_rearrange_objects();
			dial_champi_prend_barre();	
			global.champi_need_barre = 3;			
			}
		}
	}
	
if (global.champi_need_chaussures == 2) or (global.champi_need_chaussures == 1)
	{
	if (instance_exists(Sport_chaussure))
		{
		if (Sport_chaussure.taken == 2)
			{
			global.destroy_sport_chaussure = 1;
			sc_destroy_sport_chaussure();
			sc_rearrange_objects();
			dial_champi_prend_chaussure();	
			global.champi_need_chaussures = 3;			
			}
		}
	}	

// récapitulatif des équipements sportifs manquants.
if (global.champi_need_eau < 3)
and (global.champi_need_chaussures < 3)
and (global.champi_need_barre < 3)
and (global.champi_connaissance_du_probleme == 0)
	{
	dial_champi_no_effort_no_wise();	
	}
else if (global.champi_need_eau < 3)
and (global.champi_need_chaussures < 3)
and (global.champi_need_barre < 3)
and (global.champi_connaissance_du_probleme = 1)
	{
	dial_champi_no_effort_but_wise();
	}
	
else if (global.champi_need_eau == 3)
and (global.champi_need_chaussures == 3)
and (global.champi_need_barre == 3)
	{
	global.champi_need_eau = 4; // pour sortir des boucles sur l'équipement sportif
	global.champi_need_chaussures = 4;
	global.champi_need_barre =4;
	global.champi_equipement_sportif = 1;	
	if (global.champi_sport_metal_taken == 0)
		{
		global.champi_sport_metal_taken = 1;
			
		global.champi_connaissance_du_probleme = 1;
		global.champi_effort_already_done = 1;
		if (global.champi_ventoline_et_chambre_inhalation_taken == 0)
			{
			global.champi_traitement_trop_tard = 1;
			}
		}
	}
else if (global.champi_need_eau < 3)
and (global.champi_need_chaussures == 3)
and (global.champi_need_barre == 3)
	{
	dial_champi_need_eau();
	}
else if (global.champi_need_eau == 3)
and (global.champi_need_chaussures < 3)
and (global.champi_need_barre == 3)
	{
	dial_champi_need_chaussure();
	}
else if (global.champi_need_eau == 3)
and (global.champi_need_chaussures == 3)
and (global.champi_need_barre < 3)
	{
	dial_champi_need_barre();
	}
else if (global.champi_need_eau < 3)
and (global.champi_need_chaussures < 3)
and (global.champi_need_barre == 3)
	{
	dial_champi_need_chaussure_eau();
	}
else if (global.champi_need_eau == 3)
and (global.champi_need_chaussures < 3)
and (global.champi_need_barre < 3)
	{
	dial_champi_need_chaussure_barre();
	}
else if (global.champi_need_eau < 3)
and (global.champi_need_chaussures == 3)
and (global.champi_need_barre < 3)
	{
	dial_champi_need_barre_eau();
	}

	
	
// dialogue en rapport avec l'asthme...
if (global.champi_equipement_sportif == 1)
	{
	// pnj prend le bon traitement
	// vento et chambre d'inhalation combinée
	if (global.champi_ventoline_et_chambre_inhalation_taken == 0)
		{
		if (instance_exists(Vento_et_chambre_inhalation))
			{
			if (instance_exists(global.combo_vento_cdi_1))
				{
				if (global.combo_vento_cdi_1.taken == 2)
					{
					if (global.champi_connaissance_du_probleme == 1)
						{
						global.destroy_vento_et_chambre_inhalation = 1;
						if (audio_is_playing(sd_asthma_attack)) {audio_stop_sound(sd_asthma_attack);}
						sc_destroy_vento_et_chambre_inhalation(global.combo_vento_cdi_1);
						sc_rearrange_objects();
						global.champi_ventoline_et_chambre_inhalation_taken = 1;
						}
					}
				}
			if (instance_exists(global.combo_vento_cdi_2))
				{		
				if (global.combo_vento_cdi_2.taken == 2)
					{
					if (global.champi_connaissance_du_probleme == 1)
						{
						global.destroy_vento_et_chambre_inhalation = 1;
						if (audio_is_playing(sd_asthma_attack)) {audio_stop_sound(sd_asthma_attack);}
						sc_destroy_vento_et_chambre_inhalation(global.combo_vento_cdi_2);
						sc_rearrange_objects();
						global.champi_ventoline_et_chambre_inhalation_taken = 1;
						}
					}
				}
			}
		}	
	
	// pnj remonte temps.
	if (global.champi_remonte_temps_taken == 0)
		{
		if (instance_exists(Remonte_temps))
			{
			if (Remonte_temps.taken == 2)
				{
				// detruction de tout les objets
				audio_play_sound(sd_time_travel,10,0);
				audio_sound_pitch(sd_time_travel,1.5);
				sc_reinit_all_objects("kill");
				sc_room_check_destroy();
				sc_init_global_variables_pnj();
				
				global.champi_equipement_sportif = 1;
				global.champi_need_eau = 4; // pour sortir des boucles sur l'équipement sportif
				global.champi_need_chaussures = 4;
				global.champi_need_barre =4;				
				global.champi_remonte_temps_taken = 1;
				
				global.champi_effort_already_done = 0;
				global.champi_connaissance_du_probleme = 1;
				global.champi_ventoline_et_chambre_inhalation_taken = 0;
			
				Cache_transition_room.state = "come on";
				Cache_transition_room.target = room;
				Cache_transition_room.facteur_fade = 0.005;
				
				connaissance ++;
				//room_restart();
				}
			}
		}	

	
	// dialogues
	// etat initial
	//if (global.champi_effort_already_done == 0) and (global.champi_connaissance_du_probleme == 0)
	//	{
	//	dial_champi_no_effort_no_wise();	
	//	}
	
	if (connaissance == 0) // juste utilisé par le pouloeuf
		{
		if (global.champi_effort_already_done == 1) and (global.champi_connaissance_du_probleme == 1)
		and (global.champi_ventoline_et_chambre_inhalation_taken == 0)
		and (global.champi_traitement_trop_tard == 1)
			{
			dial_champi_asthmatique_trop_tard();
			global.champi_need_pouloeuf = 1;
			}
	
		if (global.champi_effort_already_done == 1) and (global.champi_connaissance_du_probleme == 1)
		and (global.champi_ventoline_et_chambre_inhalation_taken == 1)
		and (global.champi_traitement_trop_tard == 1)
			{
			dial_champi_traitement_peu_efficace();	
			}
	
		if (global.champi_effort_already_done == 0) and (global.champi_connaissance_du_probleme == 1)
		and (global.champi_ventoline_et_chambre_inhalation_taken == 0)
			{
			dial_champi_reclame_traitement();	
			}

		if (global.champi_effort_already_done == 0) and (global.champi_connaissance_du_probleme == 1) 
		and (global.champi_ventoline_et_chambre_inhalation_taken == 1)
		and (global.champi_traitement_trop_tard = 0)
			{
			dial_champi_prend_traitement();	
			global.champi_equipement_sportif = 0;
			global.champi_need_eau = 1; // pour sortir des boucles sur l'équipement sportif
			global.champi_need_chaussures = 1;
			global.champi_need_barre =1;		
			}
	
		if (global.champi_equipement_sportif == 1) and (global.champi_connaissance_du_probleme == 1) 
		and (global.champi_ventoline_et_chambre_inhalation_taken == 1)
		and (global.champi_traitement_trop_tard == 0)
			{
			global.champi_portrait = sp_portrait_Champi_content;			
			dial_champi_sport_agreable();	
			global.champi_success = 1;
			}
		}
	}	