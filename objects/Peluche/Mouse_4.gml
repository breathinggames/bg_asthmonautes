/// @description Insert description here


// change state selon objects
// ventoline seule

// présentation des objets
if (instance_exists(Mayo))
	{
	if (Mayo.taken == 2)
		{
		dial_peluche_objet_mayo();
		}
	}

if (instance_exists(De_12))
	{
	if (De_12.taken == 2)
		{
		dial_peluche_objet_de_12();
		}
	}
	
if (instance_exists(Certificat))
	{
	if (Certificat.taken == 2)
		{
		dial_peluche_objet_certificat();
		}
	}
	
if (instance_exists(Parchemin))
	{
	if (Parchemin.taken == 2)
		{
		dial_peluche_objet_parchemin();
		}
	}
	
if (instance_exists(Trophee))
	{
	if (Trophee.taken == 2)
		{
		dial_peluche_objet_trophee();
		}
	}
	
if (instance_exists(Flovent))
	{
	if (Flovent.taken == 2)
		{
		dial_peluche_objet_flovent();
		}
	}
	
if (instance_exists(Antibiotique))
	{
	if (Antibiotique.taken == 2)
		{
		dial_peluche_objet_antibiotique();
		}
	}
	
if (instance_exists(Soupe))
	{
	if (Soupe.taken == 2)
		{
		dial_peluche_objet_soupe();
		}
	}
	
if (instance_exists(Tylenol))
	{
	if (Tylenol.taken == 2)
		{
		dial_peluche_objet_tylenol();
		}
	}
	
if (instance_exists(Eau))
	{
	if (Eau.taken == 2)
		{
		dial_peluche_objet_eau();
		}
	}

if (instance_exists(Remonte_temps))
	{
	if (Remonte_temps.taken == 2)
		{
		dial_peluche_objet_remonte_temps();
		}
	}

if (instance_exists(Sport_metal))
	{
	if (Sport_metal.taken == 2)
		{
		dial_peluche_objet_sport_metal();
		}
	}

if (instance_exists(Sport_chaussure))
	{
	if (Sport_chaussure.taken == 2)
		{
		dial_peluche_objet_sport_chaussure();
		}
	}
// fin dialogue de base

if (global.peluche_ventoline_taken == 0)
	{
	if (instance_exists(Ventoline_1))
		{
		if (Ventoline_1.taken == 2)
			{
			global.peluche_ventoline_taken = 1;
			}
		}
	if (instance_exists(Ventoline_2))
		{
		if (Ventoline_2.taken == 2)
			{
			global.peluche_ventoline_taken = 1;
			}
		}
	if (instance_exists(Ventoline_3))
		{
		if (Ventoline_3.taken == 2)
			{
			global.peluche_ventoline_taken = 1;
			}
		}		
	}
	
// chambre d'inhalation seule
if (global.peluche_Chambre_inhalation_1_taken == 0)
	{
	if (instance_exists(Chambre_inhalation_1))
		{
		if (Chambre_inhalation_1.taken == 2)
			{
			global.peluche_Chambre_inhalation_1_taken = 1;
			}
		}
	if (instance_exists(Chambre_inhalation_2))
		{
		if (Chambre_inhalation_2.taken == 2)
			{
			global.peluche_Chambre_inhalation_1_taken = 1;
			}
		}		
	}	
	
// vento et chambre d'inhalation combinée
if (global.peluche_ventoline_et_Chambre_inhalation_1_taken == 0)
	{
	if (instance_exists(Vento_et_chambre_inhalation))
		{
		if (instance_exists(global.combo_vento_cdi_1))
			{
			if (global.combo_vento_cdi_1.taken == 2)
				{
				global.destroy_vento_et_chambre_inhalation = 1;
				if (audio_is_playing(sd_asthma_attack)) {audio_stop_sound(sd_asthma_attack);}
				sc_destroy_vento_et_chambre_inhalation(global.combo_vento_cdi_1);
				sc_rearrange_objects();
				global.peluche_ventoline_et_Chambre_inhalation_1_taken = 1;
				}
			}
		else if (instance_exists(global.combo_vento_cdi_2))
			{
			if (global.combo_vento_cdi_2.taken == 2)
				{
				global.destroy_vento_et_chambre_inhalation = 1;
				if (audio_is_playing(sd_asthma_attack)) {audio_stop_sound(sd_asthma_attack);}
				sc_destroy_vento_et_chambre_inhalation(global.combo_vento_cdi_2);
				sc_rearrange_objects();
				global.peluche_ventoline_et_Chambre_inhalation_1_taken = 1;
				}
			}
		}
	}	
	
// anti_histaminique
if (global.peluche_anti_allergique_taken == 0)
	{
	if (instance_exists(Anti_allergique))
		{
		if (Anti_allergique.taken == 2)
			{
			global.destroy_anti_allergique = 1;
			sc_destroy_anti_allergique();
			sc_rearrange_objects();
			global.peluche_anti_allergique_taken = 1;
			}
		}
	}	
		
if (global.champi_need_chaussures == 1)
	{
	dial_peluche_donne_chaussure();
	global.champi_need_chaussures = 2;
	instance_create_depth(x,y+75,depth, Sport_chaussure);
	}
	
	
// dialogues
// full cured
if (global.peluche_ventoline_et_Chambre_inhalation_1_taken == 1) and (global.peluche_anti_allergique_taken == 1)
	{
	portrait = sp_portrait_Peluche_content;		
	dial_peluche_full_cured();	
	global.peluche_success = 1;	
	if (global.urne_bois_statut == "closed")
		{
		global.urne_bois_statut = "opening";
		}
	}
	
else
	{
	// full sick
	if (global.peluche_ventoline_taken == 0) and (global.peluche_Chambre_inhalation_1_taken == 0)
	and (global.peluche_ventoline_et_Chambre_inhalation_1_taken == 0) and (global.peluche_anti_allergique_taken == 0)
		{
		dial_peluche_full_sick();	
		if (global.peluche_give_parchemin == 0)
			{
			instance_create_depth(x,y+75, depth, Parchemin);
			global.peluche_give_parchemin = 1;
			}
		}

	// half cured, still need vento + c-d'I
	if (global.peluche_ventoline_taken == 0) and (global.peluche_Chambre_inhalation_1_taken == 0)
	and (global.peluche_ventoline_et_Chambre_inhalation_1_taken == 0) and (global.peluche_anti_allergique_taken == 1)
		{
		dial_peluche_still_need_vento();	
		}
	
	// half cured, still need anti-hista
	if (global.peluche_ventoline_taken == 0) and (global.peluche_Chambre_inhalation_1_taken == 0)
	and (global.peluche_ventoline_et_Chambre_inhalation_1_taken == 1) and (global.peluche_anti_allergique_taken == 0)
		{
		dial_peluche_still_need_traitement();	
		}

	// half cured, refuse la vento seule
	if (global.peluche_ventoline_taken == 1) and (global.peluche_Chambre_inhalation_1_taken == 0)
		{
		dial_peluche_refuse_vento_seule();
		global.peluche_ventoline_taken = 0;
		}
	
	// half cured, refuse la C-d'I seule
	if (global.peluche_ventoline_taken == 0) and (global.peluche_Chambre_inhalation_1_taken == 1)
		{
		dial_peluche_refuse_c_d_i_seule();	
		global.peluche_Chambre_inhalation_1_taken = 0;
		}	
	}
	