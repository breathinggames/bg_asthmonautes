	

var connaissance = 0;

// je donne l'ordonnance certificat.
if (global.ordonnance_absorbee == 0)
	{
	// ouverture
	if (instance_exists(Certificat))
		{	
		if (Certificat.taken == 2)
			{
			global.destroy_certificat = 1;
			dial_distributeur_certificat();
			sc_destroy_certificat();
			sc_rearrange_objects();
			connaissance++;
			global.ordonnance_absorbee = 1;
			}
		}
	}
	
// je donne l'ordonnance parchemin, pour l'accès aux chambre d'inhalation.
if (global.parchemin_absorbee == 0)
	{
	// ouverture
	if (instance_exists(Parchemin))
		{	
		if (Parchemin.taken == 2)
			{
			global.destroy_parchemin = 1;
			dial_distributeur_parchemin();
			sc_destroy_parchemin();
			sc_rearrange_objects();
			connaissance++;
			global.parchemin_absorbee = 1;
			}
		}
	}

if (global.ordonnance_absorbee == 1)
	{
	instance_create_depth(x,y+50,depth,Boite_01);
	connaissance++;
	global.ordonnance_absorbee = 2;
	}
	
if (global.parchemin_absorbee == 1)
	{
	instance_create_depth(x,y+50,depth,Boite_02);
	connaissance++;
	global.parchemin_absorbee = 2;
	}
		
		


// fin présentation des objets
if (connaissance == 0)
	{
	if (global.ordonnance_absorbee == 0)
		{
		dial_distributeur_description();
		}
	else
		{
		global.distributeur_texte_clic_index = dial_clic_direct(global.distributeur_texte_clic_index);
		}
	}

