/// @description Insert description here
// You can write your code in this editor


image_alpha = 0.01;
image_xscale = random_range(1,10);

en_bordure = 1;

if (Cache_transition_room.facteur_fade != Cache_transition_room.facteur_fade_init)
	{
	image_blend = make_color_hsv(irandom(255), 255, 255);
	hspeed = +random_range(25,100);
	}
else
	{
	image_blend = Cache_transition_room.my_colour;	
	hspeed = -random_range(25,100);
	}

alarm[0]=1;