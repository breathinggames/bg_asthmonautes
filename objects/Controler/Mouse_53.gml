/// @description Insert description here
// You can write your code in this editor

var nombre_de_particules;
var my_image_blend;
var my_scale;
var go_particule = "beaucoup";


x = mouse_x;
y = mouse_y;


// valeur de clic par défaut, mais ça peut changer plus bas.
nombre_de_particules = irandom_range(5,9);
my_image_blend = c_white;
my_scale = 1;


if (instance_position(x, y, Item_parent)) 
or (instance_position(x, y, Pnj_parent))
or (instance_position(x, y, Porte_parent))
or (instance_position(x, y, Bouton_parent))
or (instance_position(x, y, Flag_parent))
	{
	if instance_position(x, y, Item_parent)
		{
		my_item_taken = instance_position(x, y, Item_parent)
			{
			if (my_item_taken.taken == 2) or (my_item_taken.taken == 3) or (my_item_taken.image_alpha == 0)
				{
				go_particule = "un peu";
				// envoi du son de clic
				audio_play_sound(ds_list_find_value(global.list_fx_clic, irandom(ds_list_size(global.list_fx_clic)-1)), 10,0);
				}
			}
		}
	
	if (go_particule == "beaucoup")
		{
		nombre_de_particules = irandom_range(30,50);
		my_image_blend = make_color_hsv(irandom(255), 255, 255);
		my_scale = 1.5;
		
		// envoi du clic particulier aux bonnes interactions
		audio_play_sound(ds_list_find_value(global.list_fx_good_clic, irandom(ds_list_size(global.list_fx_good_clic)-1)), 10,0);
		}
	}
	
else
	{
	// envoi du son de clic
	audio_play_sound(ds_list_find_value(global.list_fx_clic, irandom(ds_list_size(global.list_fx_clic)-1)), 10,0);
	}

	
repeat(nombre_de_particules)
	{
	my_particule = instance_create_depth(mouse_x, mouse_y , -500, Particule);
	my_particule.image_blend = my_image_blend;
	my_particule.image_xscale = my_scale;
	my_particule.image_yscale = my_scale;
	with my_particule
		{
		motion_add(random(360),random_range(5,8))
		}
	}
	
x = 0;
y = 0;

// actions un peu particulière :
// déverouillage du mixeur par Hiboux. Je veux que le déverouillage se fasse 2 clics après le lancement de dialogue et sans interuption

if (oDialog.active == 1)
	{
	if (global.hiboux_clic_deverouillage > 0)
		{
		global.hiboux_clic_deverouillage ++;
		if (place_meeting(mouse_x,mouse_y,Pnj_parent))
			{		
			if not (place_meeting(mouse_x,mouse_y,Hiboux))
				{
				global.hiboux_clic_deverouillage = 0;
				}
			}
		if (place_meeting(mouse_x,mouse_y,Porte_parent))
			{	
			global.hiboux_clic_deverouillage = 0;
			}			
		}
	if (global.hiboux_clic_deverouillage == 4)
		{
		global.mixeur_statut = "opening";
		}
	}



