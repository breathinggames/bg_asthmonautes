/// @description Insert description here
// You can write your code in this editor

//room_goto(room_01);

// curseur souris
//cursor_sprite = sp_cursor_souris;

randomize();

conteneur_dialogues(); // chargement de tous les dialogues

x = 0;
y = 0; 

cursor_inline = instance_create_depth(x,y,-610, Curseur_inline);
cursor_outline = instance_create_depth(x,y,-600, Curseur_outline);
global.curseur_mode = "black";

cache_transition_room = instance_create_depth(0,0,-500, Cache_transition_room);
cache_ouverture_porte = instance_create_depth(0,0,-500, Cache_ouverture_porte);
vitesse_particule = 15;

seuil_mouse_spawn_inventaire = room_height / 4 * 3;


global.test = 0; // pour des tests en vrac.

global.cloud_limit = 8;

global.y_inventaire_visible = room_height - 100;
global.y_inventaire_invisible = room_height + 70;
global.y_inventaire = global.y_inventaire_visible;
global.vitesse_escamotage_inventaire = 20;


// variable pour la gestion de l'inventaire
global.count_inventory = 0;
global.inventaire_position_1 = 146;
global.inventaire_position_2 = 292;
global.inventaire_position_3 = 438;
global.inventaire_position_4 = 584;
global.inventaire_position_5 = 730;
global.inventaire_position_6 = 876;
global.inventaire_position_7 = 1022;
global.inventaire_position_8 = 1168;
global.inventaire_position_9 = 1314;
global.inventaire_position_10 = 1460;
global.inventaire_position_11 = 1606;
global.inventaire_position_12 = 1752;

// depth
global.depth_porte = -60;
global.depth_objet = -50;
global.depth_objet_taken = -100;
global.depth_boutton = -50;
global.depth_credits = 50;
global.depth_iles = 100;
global.depth_nuages = 150;


// utilisé pour repositionner les objets suite à l'utilisation de l'un d'entre eux.
sc_init_arrange_object();


// variable pour les portes
global.arbre_statut = "closed";
global.oeuf_statut = "closed";
global.urne_bois_statut = "closed";
global.urne_pierre_statut = "open";
global.rayon_statut = "open";
global.mixeur_statut = "closed";


global.door_to_unlock = noone;

global.particule_destroy_objets = 0;
sc_init_global_variables_pnj();


// listes des musiques :
global.list_music_ile_01 = ds_list_create();
global.list_music_ile_02 = ds_list_create();
global.list_music_ile_03 = ds_list_create();
global.list_music_ile_04 = ds_list_create();
global.list_music_ile_05 = ds_list_create();
global.list_music_ile_06 = ds_list_create();
global.list_music_ile_07 = ds_list_create();
global.list_music_ile_08 = ds_list_create();

global.list_fx_clic = ds_list_create();
global.list_fx_good_clic = ds_list_create();

ds_list_add(global.list_music_ile_01, music_1_a, music_1_b, music_1_c);
ds_list_add(global.list_music_ile_02, music_2_a, music_2_b, music_2_c, music_2_d);
ds_list_add(global.list_music_ile_03, music_3_a, music_3_b);
ds_list_add(global.list_music_ile_04, music_4_a, music_4_b, music_4_c);
ds_list_add(global.list_music_ile_05, music_5_a, music_5_b, music_5_c, music_5_d);
ds_list_add(global.list_music_ile_06, music_6_a, music_6_b, music_6_c, music_6_d);
ds_list_add(global.list_music_ile_07, music_7_a, music_7_b);
ds_list_add(global.list_music_ile_08, music_8_a, music_8_b, music_8_c);

ds_list_add(global.list_fx_clic, sd_clic_01, sd_clic_02, sd_clic_03, sd_clic_04, sd_clic_05, sd_clic_06, sd_clic_07, sd_clic_08, sd_clic_09);
ds_list_add(global.list_fx_good_clic, sd_good_clic_01, sd_good_clic_02, sd_good_clic_03, sd_good_clic_04);

sc_schuffle_list_music();

//juste une initialisation
global.global_bk_music_01 = ds_list_find_value(global.list_music_ile_01,0);
global.global_bk_music_02 = ds_list_find_value(global.list_music_ile_01,1);


//alarm[1] = 2; // pour lancer les musique, mais avec un petit décalage comme ça, si je prend un raccourci pour debbug, les musiques ne se lancent pas.






// shortcut
//room_goto(room_ending);
/*
global.yeti_success = 1;
global.peluche_success = 1;
global.champi_success = 1;
*/

// shortcut variables
/*
global.arbre_statut = "open";
global.oeuf_statut = "open";
global.urne_bois_statut = "open";
global.urne_pierre_statut = "open";
global.rayon_statut = "open";
global.mixeur_statut = "open";
*/

//global.mixeur_statut = "open";
