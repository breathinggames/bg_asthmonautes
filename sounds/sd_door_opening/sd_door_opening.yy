{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sd_door_opening",
  "duration": 4.454478,
  "parent": {
    "name": "Fx",
    "path": "folders/Sounds/Fx.yy",
  },
  "resourceVersion": "1.0",
  "name": "sd_door_opening",
  "tags": [],
  "resourceType": "GMSound",
}