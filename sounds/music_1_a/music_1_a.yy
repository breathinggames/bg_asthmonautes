{
  "conversionMode": 0,
  "compression": 1,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "music_1_a",
  "duration": 53.841984,
  "parent": {
    "name": "Musiques",
    "path": "folders/Sounds/Musiques.yy",
  },
  "resourceVersion": "1.0",
  "name": "music_1_a",
  "tags": [],
  "resourceType": "GMSound",
}