{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 65,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 66,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"be544caa-24f9-446b-a29b-2722c21e7d3d","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be544caa-24f9-446b-a29b-2722c21e7d3d","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"LayerId":{"name":"9e5d2abb-303f-4291-8f63-be16a335f655","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_bouton_demarrer","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","name":"be544caa-24f9-446b-a29b-2722c21e7d3d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"16daaab5-c465-4da6-a5db-924c01a3d305","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"16daaab5-c465-4da6-a5db-924c01a3d305","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"LayerId":{"name":"9e5d2abb-303f-4291-8f63-be16a335f655","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_bouton_demarrer","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","name":"16daaab5-c465-4da6-a5db-924c01a3d305","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa1d2ac7-b793-4348-9847-a1df1bd78f97","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa1d2ac7-b793-4348-9847-a1df1bd78f97","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"LayerId":{"name":"9e5d2abb-303f-4291-8f63-be16a335f655","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_bouton_demarrer","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","name":"aa1d2ac7-b793-4348-9847-a1df1bd78f97","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_bouton_demarrer","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a1ce498c-3611-459a-b08b-8ae4b2d05c44","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be544caa-24f9-446b-a29b-2722c21e7d3d","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6accba6-5d08-44fb-ab92-24b811f3e5cd","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"16daaab5-c465-4da6-a5db-924c01a3d305","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"beafe049-c0e9-4cd4-8386-18bf7209525d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa1d2ac7-b793-4348-9847-a1df1bd78f97","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 128,
    "yorigin": 65,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_bouton_demarrer","path":"sprites/sp_bouton_demarrer/sp_bouton_demarrer.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9e5d2abb-303f-4291-8f63-be16a335f655","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HUD",
    "path": "folders/Sprites/HUD.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_bouton_demarrer",
  "tags": [],
  "resourceType": "GMSprite",
}