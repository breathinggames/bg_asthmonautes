{
  "bboxMode": 1,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 503,
  "bbox_top": 0,
  "bbox_bottom": 407,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 504,
  "height": 408,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"36160b0f-9066-4f30-8093-bdf1343a70d8","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"36160b0f-9066-4f30-8093-bdf1343a70d8","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},"LayerId":{"name":"56a07b15-e31d-4759-8b62-76142c58e992","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_anti_allergique","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},"resourceVersion":"1.0","name":"36160b0f-9066-4f30-8093-bdf1343a70d8","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_anti_allergique","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3f20838f-cb3f-450c-b0eb-82f2fe7e5954","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36160b0f-9066-4f30-8093-bdf1343a70d8","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 252,
    "yorigin": 204,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_anti_allergique","path":"sprites/sp_anti_allergique/sp_anti_allergique.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"56a07b15-e31d-4759-8b62-76142c58e992","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Allergie",
    "path": "folders/Sprites/sprites items pédagogiques/Allergie.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_anti_allergique",
  "tags": [],
  "resourceType": "GMSprite",
}