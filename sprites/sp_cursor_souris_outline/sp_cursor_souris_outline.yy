{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 95,
  "bbox_top": 0,
  "bbox_bottom": 95,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 96,
  "height": 96,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"116f60cf-e928-4401-8557-17a9b14f3c02","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"116f60cf-e928-4401-8557-17a9b14f3c02","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},"LayerId":{"name":"41d989c3-cb18-491c-a445-56277a767dee","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_cursor_souris_outline","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},"resourceVersion":"1.0","name":"116f60cf-e928-4401-8557-17a9b14f3c02","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_cursor_souris_outline","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"30d06ced-feb2-4271-894f-8035114ced95","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"116f60cf-e928-4401-8557-17a9b14f3c02","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 4,
    "yorigin": 4,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_cursor_souris_outline","path":"sprites/sp_cursor_souris_outline/sp_cursor_souris_outline.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"41d989c3-cb18-491c-a445-56277a767dee","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HUD",
    "path": "folders/Sprites/HUD.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_cursor_souris_outline",
  "tags": [],
  "resourceType": "GMSprite",
}