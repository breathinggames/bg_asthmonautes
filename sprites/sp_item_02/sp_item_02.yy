{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": -41,
  "bbox_right": 226,
  "bbox_top": -36,
  "bbox_bottom": 335,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 182,
  "height": 300,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"20e4968b-70f7-4dad-a6a7-8095ac88e43c","path":"sprites/sp_item_02/sp_item_02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"20e4968b-70f7-4dad-a6a7-8095ac88e43c","path":"sprites/sp_item_02/sp_item_02.yy",},"LayerId":{"name":"db75c22e-ce9c-4ef2-9ad1-a3aaeb9d38c5","path":"sprites/sp_item_02/sp_item_02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_item_02","path":"sprites/sp_item_02/sp_item_02.yy",},"resourceVersion":"1.0","name":"20e4968b-70f7-4dad-a6a7-8095ac88e43c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_item_02","path":"sprites/sp_item_02/sp_item_02.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2ff5595a-8e3b-49ff-b0d4-710eda07a98e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"20e4968b-70f7-4dad-a6a7-8095ac88e43c","path":"sprites/sp_item_02/sp_item_02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 91,
    "yorigin": 150,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_item_02","path":"sprites/sp_item_02/sp_item_02.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"db75c22e-ce9c-4ef2-9ad1-a3aaeb9d38c5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "sprites Items pour le fun",
    "path": "folders/Sprites/sprites Items pour le fun.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_item_02",
  "tags": [],
  "resourceType": "GMSprite",
}