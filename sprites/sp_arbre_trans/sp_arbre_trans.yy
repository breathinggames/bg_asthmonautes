{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 299,
  "bbox_top": 0,
  "bbox_bottom": 243,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 300,
  "height": 256,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"65b2d481-7c97-47e5-b223-32f4a94aa809","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65b2d481-7c97-47e5-b223-32f4a94aa809","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},"LayerId":{"name":"33e35fb5-bf49-4e7e-8a1c-af61f41241db","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_arbre_trans","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},"resourceVersion":"1.0","name":"65b2d481-7c97-47e5-b223-32f4a94aa809","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_arbre_trans","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ceb5652b-c399-4117-a42f-d0c5d1e4f0cf","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65b2d481-7c97-47e5-b223-32f4a94aa809","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_arbre_trans","path":"sprites/sp_arbre_trans/sp_arbre_trans.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"33e35fb5-bf49-4e7e-8a1c-af61f41241db","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Éléments de décors",
    "path": "folders/Sprites/Éléments de décors.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_arbre_trans",
  "tags": [],
  "resourceType": "GMSprite",
}