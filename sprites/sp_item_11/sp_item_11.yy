{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 325,
  "bbox_top": 16,
  "bbox_bottom": 299,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 326,
  "height": 301,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"46a7631c-3253-4263-bfd9-e0509df3b3e2","path":"sprites/sp_item_11/sp_item_11.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"46a7631c-3253-4263-bfd9-e0509df3b3e2","path":"sprites/sp_item_11/sp_item_11.yy",},"LayerId":{"name":"ec2dbe29-26aa-4b3b-a550-756d6ff3c1d2","path":"sprites/sp_item_11/sp_item_11.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_item_11","path":"sprites/sp_item_11/sp_item_11.yy",},"resourceVersion":"1.0","name":"46a7631c-3253-4263-bfd9-e0509df3b3e2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_item_11","path":"sprites/sp_item_11/sp_item_11.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6b3e45ae-c49c-4f96-b6b8-fefb4da9b4ad","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"46a7631c-3253-4263-bfd9-e0509df3b3e2","path":"sprites/sp_item_11/sp_item_11.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 163,
    "yorigin": 150,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_item_11","path":"sprites/sp_item_11/sp_item_11.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ec2dbe29-26aa-4b3b-a550-756d6ff3c1d2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "sprites Items pour le fun",
    "path": "folders/Sprites/sprites Items pour le fun.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_item_11",
  "tags": [],
  "resourceType": "GMSprite",
}