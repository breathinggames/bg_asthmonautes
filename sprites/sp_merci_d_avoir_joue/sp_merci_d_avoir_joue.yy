{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 1046,
  "bbox_top": 11,
  "bbox_bottom": 149,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1048,
  "height": 168,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8d44ec79-ecb7-4c9d-a6ce-49bdfbabd06f","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8d44ec79-ecb7-4c9d-a6ce-49bdfbabd06f","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},"LayerId":{"name":"219a2458-911e-4e08-af55-68347bfd3487","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sp_merci_d_avoir_joue","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},"resourceVersion":"1.0","name":"8d44ec79-ecb7-4c9d-a6ce-49bdfbabd06f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sp_merci_d_avoir_joue","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"48173250-f55c-4dac-b0eb-2857c04f8756","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8d44ec79-ecb7-4c9d-a6ce-49bdfbabd06f","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 524,
    "yorigin": 84,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sp_merci_d_avoir_joue","path":"sprites/sp_merci_d_avoir_joue/sp_merci_d_avoir_joue.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"219a2458-911e-4e08-af55-68347bfd3487","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HUD",
    "path": "folders/Sprites/HUD.yy",
  },
  "resourceVersion": "1.0",
  "name": "sp_merci_d_avoir_joue",
  "tags": [],
  "resourceType": "GMSprite",
}